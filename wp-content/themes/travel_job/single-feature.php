<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package travel_job
 */

get_header();
?>

<div class="main-wid">
  <div class="pan">
    <a href="<?php bloginfo('url'); ?>">TOP</a> > <a href="<?php bloginfo('url'); ?>/feature">特集記事一覧</a> > <?php the_title(); ?>
  </div>
  <div class="ftrWrap">
    <?php if ( have_posts() ): ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="ftrTtl">
                <h1><?php the_title(); ?></h1>
            </div>
            <?php the_content(); ?>
        <?php endwhile; ?>
    <?php endif; ?>
  </div>
</div>
<section class="ftrOther">
  <div class="sectionTitle balloon1">
    <h2>他にもこちらのインタビュー記事が読まれています。</h2>
  </div>
  <div class="ftr_other">
    <?php $args = array(
        'post_type'=>'feature',
        'posts_per_page'=>4,
        'orderby'=>'date',
        'order'=>'DESC',
        );
        $posts = get_posts( $args );
        if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
        <div class="ftr_otherInr">
          <a href="<?php the_permalink(); ?>">
            <div class="ftrThumb">
			  <figure>
                <?php the_post_thumbnail('medium'); ?>
              </figure>
              <div class="ftrPara">
                <h3><?php echo mb_substr(strip_tags($post-> post_title),0,30) . '...'; ?></h3>
				<hr class="ftrParaHr">
                <div class="ftrParaInr">
                  <?php echo mb_substr(strip_tags($post-> post_content),0,38) . '...'; ?>
                </div>
              </div>
			</div>
          </a>
        </div>
    <?php endforeach; ?>
    <?php endif; wp_reset_postdata(); ?>
  </div>
</section>

<?php

get_footer();