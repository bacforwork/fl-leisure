<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style>
  .search-overlay{
    width: 33.33%;
    float: left;
    height: auto;
  }
  .search-overlay:not(:first-child){
    padding-left: 5px;
  }
  .search-overlay-bottom .search-overlay-sort-btn button {
    display: block;
    width: 100%;
    padding: 12px 8px;
    background: #fd817f;
    border-radius: 4px;
    color: white;
    font-size: 14px;
    font-weight: bold;
    text-align: center;
  }
</style>
<div class="searchbox clearfix">
  <form method="get" id="searchform" action="<?php bloginfo('url'); ?>" onsubmit="return customizeData()">
    <div class="search-overlay">
      <div class="search-overlay-ttl">
        <div class="overlay-close"><i class="material-icons">chevron_left</i> 戻る</div>
        <h2 class="ta-c">雇用形態 / 業種職種を選ぶ</h2>
      </div>
      <!-- employee -->
      <?php
        $taxonomy_name = 'employee';
        $taxonomys = get_terms( array( 'taxonomy' => $taxonomy_name, 'hide_empty' => true ) );
      ?>
      <div class="search-overlay-box" rel="checkbox_array">
        <?php
          if (!is_wp_error($taxonomys) && count($taxonomys)) :
        ?>
        <h3>雇用形態<span>を選ぶ</span></h3>
        <ul>
          <?php
            foreach ($taxonomys as $taxonomy) :
              $tax_posts = get_posts(array('post_type' => get_post_type('detail'), 'taxonomy' => $taxonomy_name, 'term' => $taxonomy->slug));
              if ($tax_posts) :
          ?>
            <li>
              <label>
                <input type="checkbox" name="employee_array" value="<?php echo $taxonomy->slug; ?>" rel="employee_checked"/><?php echo $taxonomy->name; ?>
              </label>
            </li>
          <?php
              endif;
            endforeach;
          ?>
        </ul>
        <?php
          endif;
        ?>
        <input type="hidden" name="employee">
      </div>
      <!-- jobtype -->
      <?php
        $taxonomy = 'jobtype';
        $parents = get_terms( array( 'taxonomy' => $taxonomy, 'parent' => 0, 'hide_empty' => true ) );
      ?>
      <div class="search-overlay-box" rel="checkbox_array">
        <h3>業種・職種<span>を選ぶ</span></h3>
        <div class="accbox">
          <?php foreach ($parents as $parent) : ?>
            <input type="checkbox" id="label<?php echo $parent->slug; ?>" class="cssacc"/>
            <label for="label<?php echo $parent->slug; ?>" class="arrow"><?php echo $parent->name; ?><span>(<?php echo $parent->count;?>件)</span></label>
            <div class="accshow">
              <ul>
                <?php
                  $children = get_term_children($parent->term_id, $taxonomy);
                  foreach ($children as $child) :
                    $termC = get_term_by('id', $child, $taxonomy);
                    if ($termC->count != 0) :
                ?>
                  <li>
                    <label>
                      <input type="checkbox" name="jobtype_array" value="<?php echo $termC->slug; ?>" rel="jobtype_checked" /><?php echo $termC->name; ?><span>(<?php echo $termC->count;?>件)</span>
                    </label>
                    <span class="input-checked">選択中</span>
                  </li>
                <?php
                    endif;
                  endforeach;
                ?>
              </ul>
            </div>
          <?php endforeach;?>
          <input type="hidden" name="jobtype">
        </div><!--//.accbox-->
      </div>
      <div class="search-overlay-bottom">
        <div class="search-count">
          <p>求人数</p>
          <span>0</span>件
        </div>
        <div class="search-overlay-back-btn">
          <a href="">戻る</a>
        </div>
        <div class="search-overlay-sort-btn">
          <button type="submit">絞り込み</button>
        </div>
      </div>
    </div>

    <div class="search-overlay">
      <div class="search-overlay-ttl">
        <div class="overlay-close"><i class="material-icons">chevron_left</i> 戻る</div>
        <h2 class="ta-c">勤務地エリアを選ぶ</h2>
      </div>
      <!-- area -->
      <?php
        $taxonomy = 'area';
        $parents = get_terms( array( 'taxonomy' => $taxonomy, 'parent' => 0, 'hide_empty' => true ) );
      ?>
      <div class="search-overlay-box" rel="checkbox_array">
        <div class="accbox">
          <?php foreach ($parents as $parent) : ?>
            <input type="checkbox" id="label<?php echo $parent->slug;?>" class="cssacc"/>
            <label for="label<?php echo $parent->slug;?>" class="arrow"><?php echo $parent->name;?><span>(<?php echo $parent->count;?>件)</span></label>
            <div class="accshow">
              <ul>
                <?php
                  $children = get_term_children($parent->term_id, $taxonomy);
                  foreach ($children as $child) :
                    $termC = get_term_by('id', $child, $taxonomy);
                    if ($termC->count != 0) :
                ?>
                  <li>
                    <label>
                      <input type="checkbox" name="area_array" value="<?php echo $termC->slug;?>" rel="area_checked"/><?php echo $termC->name;?><span>(<?php echo $termC->count;?>件)</span>
                    </label>
                  </li>
                <?php
                    endif;
                  endforeach;
                ?>
              </ul>
            </div>
          <?php endforeach;?>
          <input type="hidden" name="area">
        </div><!--//.accbox-->
      </div>
      <div class="search-overlay-bottom">
        <div class="search-count">
          <p>求人数</p>
          <span>0</span>件
        </div>
        <div class="search-overlay-back-btn">
          <a href="">戻る</a>
        </div>
        <div class="search-overlay-sort-btn">
          <button type="submit">絞り込み</button>
        </div>
      </div>
    </div>

    <div class="search-overlay" rel="checkbox_array">
      <div class="search-overlay-ttl">
        <div class="overlay-close"><i class="material-icons">chevron_left</i> 戻る</div>
        <h2 class="ta-c">こだわり条件を選ぶ</h2>
      </div>
      <?php
        $condition_filter = array(
          '人材' => ['beginner', 'woman', 'kanbu', 'zaitaku'],
          '職場・休み' => ['accsess','holiday','short_time','speak_english'],
          '待遇' => ['commission','welfare','long_holiday','qualification'],
        );
        $taxonomy_name = 'condition';
        foreach ($condition_filter as $key => $value) :
          $taxonomys = get_terms (
            [
              'slug'     => $value,
              'taxonomy' => $taxonomy_name,
              'hide_empty' => true,
            ]
          );
          if (!is_wp_error($taxonomys) && count($taxonomys)) :
      ?>
        <div class="search-overlay-box">
          <h3><?php echo $key;?><span>のこだわり</span></h3>
          <ul>
            <?php foreach ($taxonomys as $taxonomy) : ?>
              <li>
                <label>
                  <input type="checkbox" name="condition_array" value="<?php echo $taxonomy->slug;?>" rel="condition_checked" /><?php echo $taxonomy->name;?>
                </label>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      <?php
          endif;
        endforeach;
      ?>
      <input type="hidden" name="condition">
      <div class="search-overlay-bottom">
        <div class="search-count">
          <p>求人数</p>
          <span>0</span>件
        </div>
        <div class="search-overlay-back-btn">
          <a href="">戻る</a>
        </div>
        <div class="search-overlay-sort-btn">
          <button type="submit">絞り込み</button>
        </div>
      </div>
    </div>
    <input type="hidden" name="s">
    <input type="hidden" name="post_type" value="detail">
  </form>
</div>

<script>
  $(function(){
    getRealTimeResultNumer();
  })

  window.onpageshow = function(event) {
    getInitResultNumber();
  }

  //customize query
  function customizeData() {
    $('[rel="checkbox_array"]').each(function(){
      var boxes,
          checked = [];
      boxes = $(this).find('input[type="checkbox"]');
      for(var i=0; boxes[i]; ++i){
        if($(boxes[i]).is(':checked')){
          checked.push($(boxes[i]).val());
        }
      }
      var checkedStr = checked.join();
      $(this).find('input[type="hidden"]').val(checkedStr);
    })
    $('input[type=checkbox]').attr('disabled','true');
    return true;
  }

  //get realtime result and keep the result when hit back button
  const arrays = {
    employee_checked: [],
    jobtype_checked: [],
    area_checked: [],
    condition_checked: [],
  };

  function getChecked(el){
    const arr = arrays[el.attr('rel')];
    if(el.is(':checked')){
      arr.push(el.val());
    } else {
      const position = arr.indexOf(el.val());
      if (position !== -1) {
        arr.splice(position, 1);
      }
    }
  }

  function ajaxGetNumberResult(_area,_jobtype,_employee,_condition){
    $.ajax({
      method: 'GET',
      url: '<?php bloginfo('url'); ?>',
      data: {
        get_ajax: 1,
        post_type: 'detail',
        area: _area,
        jobtype: _jobtype,
        employee: _employee,
        condition: _condition,
        s: '',
      },
    }).success(function(data) {
      $('.search-count span').each(function(){
        $(this).text(data);
      })
    })
  }

  function getRealTimeResultNumer(){
    $('input[type="checkbox"][name]').on('change', function(){
      getChecked($(this));
      ajaxGetNumberResult(arrays.area_checked.join(),arrays.jobtype_checked.join(),arrays.employee_checked.join(),arrays.condition_checked.join());
    })
  }

  function getInitResultNumber(){
    $('input[type="checkbox"][name]').each(function(){
      getChecked($(this));
    });
    ajaxGetNumberResult(arrays.area_checked.join(),arrays.jobtype_checked.join(),arrays.employee_checked.join(),arrays.condition_checked.join());
  }
  //
</script>