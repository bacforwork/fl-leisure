<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package travel_job
 */

get_header();
?>

<div class="main-wid">
	<div class="pan">
		<a href="<?php bloginfo('url'); ?>">TOP</a> > お役立ちコラム
	</div>
	<div class="main-col sp-pad marb4 ">
		<h1 class="main-col-Ttl column">お役立ちコラム</h1>
		<div class="archiveColumn_inner">
			<?php $args = array(
            'post_type'=>'column',
            'posts_per_page'=>12,
            'orderby'=>'date',
            'order'=>'DESC',
            );
            $posts = get_posts( $args );
            if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
			<div class="item"><!-- item -->
              <a href="<?php the_permalink(); ?>">
                <?php
                $image_id = get_post_thumbnail_id();
                $image_url = wp_get_attachment_image_src($image_id, true);
                ?>
                <div class="columnThumb">
				  <figure>
				    <img src="<?php echo $image_url[0]; ?>" width="100%" alt=""/>
                  </figure>
				  <div  class="columnPara">
					<div class="columnTag">
					  <?php
                        if ($terms = get_the_terms($post->ID, 'column_tag')) 
                        foreach ( $terms as $term ): // foreach ループの開始
                        ?>
                        <?php echo $term->name; ?>
                      <?php endforeach; ?>
					</div>
					<hr class="archiveColumnHr">
					<div class="columnParaInr">
				      <h3><?php $title= mb_substr($post->post_title,0,40); echo $title;?></h3>
				      <p><?php echo mb_substr(strip_tags($post-> post_content),0,40) . '...'; ?></p>
					</div>
				  </div>
				</div>
              </a>
            </div><!-- /item -->
        <?php endforeach; ?>
        <?php endif; wp_reset_postdata(); ?>
		  </div>
		<div class="pager">
			<?php global $wp_rewrite; $paginate_base = get_pagenum_link(1); if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
				$paginate_format = '';
				$paginate_base = add_query_arg('paged','%#%');
			}
			else{
				$paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
				user_trailingslashit('page/%#%/','paged');;
				$paginate_base .= '%_%';
			}
			echo paginate_links(array(
				'base' => $paginate_base,
				'format' => $paginate_format,
				'total' => $wp_query->max_num_pages,
				'mid_size' => 5,
				'current' => ($paged ? $paged : 1),
				'prev_text' => '«',
				'next_text' => '»',
			)); ?>
		</div>
	</div>
								
	<div class="side-col">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>


<?php
get_footer();