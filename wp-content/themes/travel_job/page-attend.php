<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package travel_job
 */

get_header();
?>
<div class="main-wid">
	<div class="pan marb1">
		<a href="<?php bloginfo('url'); ?>">TOP</a> > <?php the_title(); ?>
	</div>
	<div class="main-col sp-pad marb4 page-sec">
		<div class="detail-tit">
			<h1><?php the_title(); ?></h1>
		</div>
		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<img src="https://leisure-ist.com/wp-content/uploads/転職サポート画像-min.png">
				<div class="detail-catch mart2 marb2">
					<p style="text-align: center;">あらゆるレジャー業界（旅行業・ホテル旅館業）の現場を肌感覚で知っているレジャリストスタッフが徹底サポート！<br>
						だから	<strong class="yellow_line">やりたい仕事に転職できる</strong>！、<strong class="yellow_line">年収が上がる</strong>！、<strong class="yellow_line">新しいキャリアが見つかる</strong>！
				</div><br>
		      <div align="center">
				  <a href="#form"><img src="https://leisure-ist.com/wp-content/uploads/転職相談ボタン.png" alt="旅行ホテル転職相談" width="450"></a>
				  <a href="https://lin.ee/1qtaiQH"><img src="https://leisure-ist.com/wp-content/uploads/LINE相談ボタン.png" alt="LINE転職相談" width="450"></a>
		      </div>

		      <br><br><br><br>
								  
				<h2 class="detail-subtit texcen bgwht">
					転職アドバイザー 相談の流れ
				</h2>
				<div class="detail-catch mart2 marb2 texcen">
				</div>
				<div class="what-box marb2">
					<div class="float-img">
						<img src="https://leisure-ist.com/wp-content/uploads/chat_letter_mail_message_icon_127221.png" width="100%" alt="">
					</div>
					<div class="float-text">
						<div class="what-subtit">
							<strong class="blue_bold">① あなたの現状や希望を入力・送信！</strong>
						</div>
						探している仕事が見つからない。このくらい<span class="marker14">キャリア・年収をアップ</span>したい！などのご要望を、<span class="marker14">「レジャリスト チェックシート」</span>をご記入し送信してください！
					</div>
					<div class="clear"></div>
				</div>
		<div align="center">
			<strong class="blue_bold">▼</strong>
		</div>
				<div class="what-box marb2">
					<div class="float-img">
						<img src="https://leisure-ist.com/wp-content/uploads/internet_interview_network_online_icon_127213.png" width="100%" alt="">
					</div>
					<div class="float-text">
						<div class="what-subtit">
							<strong class="blue_bold">② 転職アドバイザーと面談！</strong>
						</div>
						業界に精通した転職アドバイザーがあなたのニーズを的確にキャッチアップ！<span class="marker14">ビデオ面談OK</span>なので、まだ現職にお勤めの方、日中に時間がとれない方も安心です♪
					</div>
					<div class="clear"></div>
				</div>
		<div align="center">
			<strong class="blue_bold">▼</strong>
		</div>
				<div class="what-box marb2">
					<div class="float-img">
						<img src="https://leisure-ist.com/wp-content/uploads/analytics_business_conference_presentation_icon_127218.png" width="100%" alt="">
					</div>
					<div class="float-text">
						<div class="what-subtit">
							<strong class="blue_bold">③ お仕事紹介&受かる面接アドバイス！</strong>
						</div>
						レジャリストスタッフは人事担当者だけじゃなく<span class="marker14">経営者との関係も深い</span>ため、その会社が必要としている人材を知っています！
						あなたの<span class="marker14">何をアピールすべきか</span>を一発でアドバイスします！
					</div>
					<div class="clear"></div>
				</div>
		<div align="center">
			<strong class="blue_bold">▼</strong>
		</div>
				<div class="what-box marb2">
					<div class="float-img">
						<img src="https://leisure-ist.com/wp-content/uploads/protection_secure_security_shield_icon_127203.png" width="100%" alt="">
					</div>
					<div class="float-text">
						<div class="what-subtit">
							<strong class="blue_bold">④ 面接・就業先の決定！</strong>
						</div>
						面接も徹底サポート！あなたにとって本当に合う会社か、やりたい仕事か、<span class="marker14">専門的かつ客観的</span>に最後まで寄り添います！
						複数の内定が出た場合は、レジャリストスタッフが<span class="marker14">代わりに辞退の手続き</span>を行います。
					</div>
					<div class="clear"></div>
				</div>
            <br>
				<div align="center">
				  <a href="#form"><img src="https://leisure-ist.com/wp-content/uploads/転職相談ボタン.png" alt="旅行ホテル転職相談" width="450"></a>
				  <a href="https://lin.ee/1qtaiQH"><img src="https://leisure-ist.com/wp-content/uploads/LINE相談ボタン.png" alt="LINE転職相談" width="450"></a>
		      </div>

	 　       <p id="form"></p>
	　　　　　　　　　　　<br>
　　　　　　　　　　　　　　　<br>
　　　　　　　　　　　　　　　<br>　
		      <br>
		
　　　	　　　　　　　　　　　<h3 class="detail-subtit texcen mart4 marb2 bgwht">
				<p style="text-align: center;">カンタン<font color="#ffff30">2分</font>入力♪<br>転職アドバイザーに相談する。</p>	
	         </h3>
		
				<div id="form"></div>
				<?php echo do_shortcode('[contact-form-7 id="865" title="転職相談登録フォーム2"]'); ?> <br> <br>
			<?php endwhile; ?>
		<?php endif; ?> <br>
	</div>
	 <br> <br> <br>						
	<div class="side-col">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>


<?php
get_footer();
