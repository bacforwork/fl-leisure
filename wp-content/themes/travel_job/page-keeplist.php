<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package travel_job
 */

get_header();
?>
<div class="main-wid">
	<div class="pan marb1">
		<a href="<?php bloginfo('url'); ?>">TOP</a> > <?php the_title(); ?>
	</div>
	<div class="main-col sp-pad marb4 page-sec">
		<div class="detail-tit">
			<h1><?php the_title(); ?></h1>
        </div>
        <div id="jobs">
        </div>
	</div>

	<div class="side-col">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>

<script type="text/javascript">
(function($) {
    var keeplist = localStorage.getItem('keeplist');
    if (!keeplist) {
        keeplist = [];
    } else {
        keeplist = JSON.parse(keeplist);
    }

    $.ajax({
        type: 'POST',
        url: '<?php echo admin_url('admin-ajax.php');?>',
        dataType: "json", // add data type
        data: {
            action : 'get_keeplist',
            keeplist: keeplist,
            nonce: '<?php echo wp_create_nonce( 'keeplist' ); ?>'
        },
        success: function( response ) {
            localStorage.setItem('keeplist', JSON.stringify(response.keeplist));
            $.each( response.posts, function( k, v ) {
                $('#jobs').append(v);
            } );
        }
    });
})(jQuery);
</script>
<?php
get_footer();
