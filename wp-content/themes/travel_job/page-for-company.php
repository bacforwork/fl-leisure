<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package travel_job
 */

get_header();
?>
<div class="forcom-bg">
	<div class="forcom-wid">
		<div class="forcom-flo">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/img/forcom/for01.png" width="100%" alt="日本最大級の旅行関連会社求人サイトレジャリスト" class="marb4">
			<div class="forcom-flo50">
				<a href="#form"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/forcom/for02.png" width="100%" alt="資料請求はこちら"></a>
			</div>
			<div class="forcom-flo50">
				<a href="#form"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/forcom/for03.png" width="100%" alt="お申込みはこちら"></a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="forcom-flo2">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/img/forcom/for04.png" width="100%" alt="掲載料0円">
		</div>
		<div class="clear marb4"></div>
		<img src="<?php echo get_stylesheet_directory_uri() ?>/img/forcom/for05.png" width="100%" alt="こんなことでお困りではありませんか？" class="marb6 pcv">
		<img src="<?php echo get_stylesheet_directory_uri() ?>/img/forcom/for05sp.png" width="100%" alt="こんなことでお困りではありませんか？" class="marb6 spv">

		<div class="forcon-tit">
			お任せください！<br>
			その悩み『レジャリスト』なら解決できます！<br>
		</div>
		<div align="center">
			<div class="forcom-orange mart2">
				解決できる理由
			</div>
		</div>

		<div class="forcom-box marb2">
			<div class="forcom-img">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/img/forcom/for06.jpg" width="100%" alt="">
			</div>
			<div class="forcom-text">
				<div class="forcom-subtit">
					その１ 本気で旅行会社・レジャー業界で働きたい人材が集まります！
				</div>
				『レジャリスト』は、旅行会社・レジャー業界に特化した求人サイトなので、業界好きのやる気のある人材がたくさん集まります！
			</div>
			<div class="clear"></div>
		</div>
		<div class="forcom-box marb2">
			<div class="forcom-img">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/img/forcom/for06.jpg" width="100%" alt="">
			</div>
			<div class="forcom-text">
				<div class="forcom-subtit">
					その２ レジャリストスタッフが事前に求職者とコンタクトを取るから安心！
				</div>
				応募が来た人材に対して、『レジャリスト』のスタッフが事前に電話やメール、LINEなどで、経験・キャリア、人間性などを把握します！だから安心！
			</div>
			<div class="clear"></div>
		</div>
		<div class="forcom-box marb2">
			<div class="forcom-img">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/img/forcom/for06.jpg" width="100%" alt="">
			</div>
			<div class="forcom-text">
				<div class="forcom-subtit">
					その３ 面接→採用→勤務開始までを、レジャリストスタッフがサポート！
				</div>
				求職者との面接調整を『レジャリスト』のスタッフが、求職者としっかりやりとりをして調整するので、面接までのやり取りの時間の無駄や労力の削減につなげることができるから効率的です！
			</div>
			<div class="clear"></div>
		</div>
		<div class="forcom-box marb2">
			<div class="forcom-img">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/img/forcom/for06.jpg" width="100%" alt="">
			</div>
			<div class="forcom-text">
				<div class="forcom-subtit">
					その４ 驚異のマッチング率！必要な人材を的確にご紹介することが可能です！
				</div>
				『レジャリスト』のスタッフは、旅行会社やレジャー業界での経験が豊富で、職場を肌感覚で理解しているため、貴社の事業内容を瞬時に理解し、必要な人材を的確に見つけ出します！
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<div class="forcom-wid">
	<div class="forcon-tit2">
		料金は完全成果報酬型だから安心！
	</div>
	<div class="forcom-center">
		登録&掲載は完全無料！採用時にのみ料金を支払う、成果報酬型の人材採用メディアです。<br>
		人材の採用にかかる工数は、たった30分間の取材・ヒアリングのみ！リスクは一切ございません！<br>
		※ヒアリングは、直接ご訪問し取材する方法と、TVミーティングでのインタビュー対応も可能です。お気軽にご相談ください。
	</div>
	<img src="<?php echo get_stylesheet_directory_uri() ?>/img/forcom/for07.png" width="100%" alt="求人メディア『レジャリスト』は、旅行業・レジャー業界が好きな本気で働きたい方にだけ" class="mart6 marb6">
	<br>
	<br>
	<div class="forcon-tit2">
		採用までの流れ
	</div>
	<div class="forcom-center">
		"求人ヒアリング〜問合せ/面接〜採用まで、『レジャリスト』はしっかりサポートします！<br>
		採用に向けての打合せから、募集要項の掲載までは最短で3日間以内のスピード感！"
	</div>
	<img src="<?php echo get_stylesheet_directory_uri() ?>/img/forcom/for08.png" width="100%" alt="" class="mart6 marb4">

	<div id="form"></div>
	<?php echo do_shortcode('[contact-form-7 id="866" title="求人・広告掲載ご希望の企業様 専用フォーム"]'); ?>
				



</div>


<?php
get_footer();
