<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package travel_job
 */

?>
<!doctype html>
<html lang="ja">
	<link rel="shortcut icon" href="https://leisure-ist.com/wp-content/uploads/レジャリス（背景黄丸）.icn_.png">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/css/top.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/css/style.css" media="screen" />
	<?php if ( !is_home() && !is_front_page() ) : ?>
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/css/style-kaso.css" media="screen" />
	<?php endif; ?>
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/css/common.css" media="all">
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/slick/slick.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/slick/slick-theme.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/css/remodal.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri() ?>/css/remodal-default-theme.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri() ?>/slick/slick.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri() ?>/js/infiniteslidev2.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri() ?>/js/top.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri() ?>/js/remodal.min.js"></script>

	<?php wp_deregister_script('jquery'); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="wrapper">
  <div id="content">
    <!-- header -->
	<header id="header">
		<div class="pc-header">
          <div>
            <p class="headerLogo">
              <a href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/logo.png" alt="業界初、レジャー関連求人情報サイト、LEISURist（レジャリスト）"></a>
            </p>
            <div class="tagline">
              <span><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/icon_tagline.png"></span>
				  <h1>
					  <span>旅行会社・ホテル旅館</span>のお仕事専門！<br>レジャー業界の<span>転職・求人サイト</span>
            </h1>
			  </div>
            <dl class="btnHeaderTel">
				<dt>お電話でのお問い合わせ</dt>
				<dd class="arial"><a href="tel:0357517144">03-5751-7144</a></dd>
            </dl>
            <div class="btnHeaderForm">
              <a class="btnHeaderFormInr" href="/contact">フォーム</a>
            </div>
            <div class="btnHeaderKeep">
              <a class="btnHeaderKeepInr" href="/keeplist">キープ中</a>
            </div>
		  </div>
		  <div class="gnav pc">
				<ul class="gnavList">
					<li class="gnavInner gnavDetail"><a href="/detail"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/iconDetail.svg"></span>求人情報を探す</a></li>
					<li class="gnavInner gnavFeature"><a href="/feature"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/iconFeature.svg"></span>インタビューを読む</a></li>
					<li class="gnavInner gnavColumn"><a href="/column"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/iconColumn.svg"></span>お役立ちコラムを読む</a></li>
					<li class="gnavInner gnavAttend"><a href="/attend"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/iconAttend.svg"></span>転職アドバイザーに相談</a></li>
					<li class="gnavInner gnavService"><a href="/service"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/iconService.svg"></span>求人掲載をお考えの方</a></li>
				</ul>
			</div>
    </div>
		<div class="sp-header">
          <div class="tagline">
            <h1>
				<span>旅行会社・ホテル旅館のお仕事専門！</span>レジャー業界の<span>転職・求人サイト</span>
            </h1>
          </div>
          <p class="headerLogo">
            <a href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/logo.png" alt="業界初、レジャー関連求人情報サイト、LEISURist（レジャリスト）"></a>
          </p>
          <div class="btnHeaderForm">
            <a class="btnHeaderFormInr" href="/contact"></a>
          </div>
          <div class="btnHeaderKeep">
            <a class="btnHeaderKeepInr" href="/keeplist"></a>
          </div>
          <div class="gnavSp sp">
            <div class="drawer">
              <div class="navbar_toggle">
               <span class="navbar_toggle_icon"></span>
               <span class="navbar_toggle_icon"></span>
               <span class="navbar_toggle_icon"></span>
              </div>
            </div>
            <div class="menu animation">
              <ul class="gnavListSp">
								<li class="gnavInner gnavDetail"><a href="/detail"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/iconDetail.svg"></span>求人情報を探す</a></li>
								<li class="gnavInner gnavFeature"><a href="/feature"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/iconFeature.svg"></span>インタビューを読む</a></li>
								<li class="gnavInner gnavColumn"><a href="/column"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/iconColumn.svg"></span>お役立ちコラムを読む</a></li>
								<li class="gnavInner gnavAttend"><a href="/attend"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/iconAttend.svg"></span>転職アドバイザーに相談</a></li>
								<li class="gnavInner gnavService"><a href="/service"><span><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/iconService.svg"></span>求人掲載をお考えの方</a></li>
              </ul>
            </div>
          </div>
        </div>
	</header>

	  <script>
      $(function() {
       $('.navbar_toggle').on('click', function () {
        $(this).toggleClass('open');
        $('.menu').toggleClass('open');
       });
      });
      </script>
    <!-- /header -->
    <!-- main -->
    <main>
