<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package travel_job
 */

get_header();
?>
<div class="main-wid">
	<div class="pan marb1">
		<a href="<?php bloginfo('url'); ?>">TOP</a> > <?php the_title(); ?>
	</div>
	<div class="main-col sp-pad marb4 page-sec">
		<div class="detail-tit">
			<h1><?php the_title(); ?></h1>
		</div>
		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<img src="https://leisure-ist.com/wp-content/uploads/レジャリスト-資料請求ページ-min.png">
				<div class="detail-catch mart2 marb2">
					<p style="text-align: center;">「レジャリスト」は、レジャー業界（旅行業・ホテル旅館業）に特化した、求人メディア!<br>
						<strong class="yellow_line">掲載料無料・成果報酬型</strong>の人材紹介サービスです。</p>
				</div><br><br>
		
				<h2 class="detail-subtit texcen bgwht">
					「レジャリスト」 の３つの特徴とメリット
				</h2><br>
		
				<div class="what-box marb2">
					<div class="float-img">
						<img src="https://leisure-ist.com/wp-content/uploads/掲載用アイコン１-1.png">
					</div>
					<div class="float-text">
						<div class="what-subtit">
							<strong class="blue_bold">旅行業・旅館ホテル業に特化した求人</strong>
						</div>
						業界経験者、興味関心や熱意のある人材をご紹介することが可能です。
						そのため、<span class="marker14">即戦力人材</span>のご紹介や、<span class="marker14">離職率が低い</span>などの特徴があります。
					</div>
					<div class="clear"></div>
				</div><br>
		
				<div class="what-box marb2">
					<div class="float-img">
						<img src="https://leisure-ist.com/wp-content/uploads/掲載用アイコン２.png">
					</div>
					<div class="float-text">
						<div class="what-subtit">
							<strong class="blue_bold">欲しい人材とのマッチング率が高い！</strong>
						</div>
						宿泊業・旅行業の<span class="marker14">経験者・業界精通者</span>が、サービス設計〜運営を行っております。
						現場を肌感覚で理解しているため、<span class="marker14">最適な人材のご紹介・マッチング</span>が可能です。
					</div>
					<div class="clear"></div>
				</div><br>
		
				<div class="what-box marb2">
					<div class="float-img">
						<img src="https://leisure-ist.com/wp-content/uploads/掲載用アイコン３.png">
					</div>
					<div class="float-text">
						<div class="what-subtit">
							<strong class="blue_bold">掲載無料・成果報酬型！</strong>
						</div>
						採用が確定するまで一切の費用をいただかない、<span class="marker14">完全成果報酬型</span>です。
						レジャリストに求人を掲載する、<span class="marker14">リスクとデメリットはゼロ</span>です。
				    	</div>
					<div class="clear"></div>
				</div><br>

		　　　　　　　　<div align="center">
					<a href="#form">
					<div class="tensyoku-btn">
						資料を請求する 〉
					</div>
					</a>
	         </div>	<br><br>
	
				<h2 class="detail-subtit texcen mart4 marb2 bgwht">
					ご紹介できる人材と全体像
				</h2>
						<img src="https://leisure-ist.com/wp-content/uploads/スクリーンショット-2020-02-25-19.20.37.png"><br>
                 <br>
	              <p style="text-align: center;">「レジャリスト」 は業界の特有の<br> <strong class="yellow_line">「職種 と 雇用形態」 </strong>を網羅しています</p>
	　　　　　　　　　　　<br>
	　　　　　　　　　　　<br>
				<h2 class="detail-subtit texcen mart4 marb2 bgwht">
					成果報酬型 & 低コスト! だから選ばれる
				</h2>
						<img src="https://leisure-ist.com/wp-content/uploads/スクリーンショット-2020-02-25-19.24.49.png"><br>
                 <br>
	              <p style="text-align: center;">リスク・デメリットが完全に０の<strong class="yellow_line">「成果報酬プラン」 </strong>と<br>
					  定額で何人でも採用できる<strong class="yellow_line">「採用し放題プラン」</strong>から<br>お選びいただけます。</p>
　　　　　　　　　　　	　　　<br>
	　　　　　　　　　　　<br>
				<h2 class="detail-subtit texcen mart4 marb2 bgwht">
					3日で掲載開始！ ご利用の流れ
				</h2>
						<img src="https://leisure-ist.com/wp-content/uploads/スクリーンショット-2020-02-25-20.15.22.png"><br>
                 <br>
	              <p style="text-align: center;"><strong class="yellow_line">最短２営業日</strong>で求人掲載を開始できます！<br>
					  採用が決定するまでに発生する<strong class="yellow_line">費用は0円</strong>です。</p>
	 　       <p id="form"></p>
	　　　　　　　　　　　<br>
　　　　　　　　　　　　　　　<br>
　　　　　　　　　　　　　　　<br>
　　　	　　　　　　　　　　　<h3 class="detail-subtit texcen mart4 marb2 bgwht">
				<p style="text-align: center;">掲載をご検討の企業様！<br>資料請求はたったの<font color="#ffff30">30秒</font>です。</p>	
	         </h3>

		<div id="form"></div>
  	   <?php echo do_shortcode('[contact-form-7 id="866" title="資料請求フォーム"] '); ?>
	   <br><br>
	

	　　　　　　　　　　　<h2 class="detail-subtit texcen mart4 marb2 bgwht">
					求人掲載に関して よくある質問
				</h2>
				<div class="what-qa">
					<table>
						<tr>
							<th><img src="<?php echo get_stylesheet_directory_uri() ?>/img/what/what09.png" width="100%" alt=""></th>
							<td><span class="ques"><strong class="blue_bold">初期費用や面接費用はかかりますか？</strong></span></td><br>
						</tr>
						<tr>
							<th><img src="<?php echo get_stylesheet_directory_uri() ?>/img/what/what10.png" width="100%" alt=""></th>
							<td><span class="marker14">1円</span>もかかりません！</td>
						</tr>
					</table><br>
					<table class="mart2">
						<tr>
							<th><img src="<?php echo get_stylesheet_directory_uri() ?>/img/what/what09.png" width="100%" alt=""></th>
							<td><span class="ques"><strong class="blue_bold">募集要項はいくつまで掲載できますか？</strong></span></td>
						</tr><br>
						<tr>
							<th><img src="<?php echo get_stylesheet_directory_uri() ?>/img/what/what10.png" width="100%" alt=""></th>
							<td>募集要項の掲載数は<span class="marker14">無制限</span>です！雇用形態×職種×勤務地など、細かく分けて掲載することができるので、
								<span class="marker14">マッチング率が非常に高い</span>のが特徴です！</td>
						</tr>
					</table><br><br>
					<table class="mart2">
						<tr>
							<th><img src="<?php echo get_stylesheet_directory_uri() ?>/img/what/what09.png" width="100%" alt=""></th>
							<td><span class="ques"><strong class="blue_bold">途中で掲載をやめることはできますか？</strong></span></td>
						</tr>
						<tr>
							<th><img src="<?php echo get_stylesheet_directory_uri() ?>/img/what/what10.png" width="100%" alt=""></th>
							<td>もちろんです！いつでも<span class="marker14">非表示や応募終了</span>に切り替えることが可能です。</td>
						</tr>
					</table><br><br>
					<table class="mart2">
						<tr>
							<th><img src="<?php echo get_stylesheet_directory_uri() ?>/img/what/what09.png" width="100%" alt=""></th>
							<td><span class="ques"><strong class="blue_bold">採用後すぐに辞めてしまった場合の保証はありますか？</strong></span></td>
						</tr>
						<tr>
							<th><img src="<?php echo get_stylesheet_directory_uri() ?>/img/what/what10.png" width="100%" alt=""></th>
							<td><span class="marker14">2ヶ月間の保証期間</span>がございます。入社直後、やはり会社に合わなかった・馴染めなかった、などの事態の際もご安心ください。</td>
						</tr>
					</table><br><br>
					<table class="mart2">
						<tr>
							<th><img src="<?php echo get_stylesheet_directory_uri() ?>/img/what/what09.png" width="100%" alt=""></th>
							<td><span class="ques"><strong class="blue_bold">レジャリスくんをうちで採用したいです。可能ですか？</strong></span></td>
						</tr>
						<tr>
							<th><img src="<?php echo get_stylesheet_directory_uri() ?>/img/what/what10.png" width="100%" alt=""></th>
							<td>大変申し訳ございません、それだけはできかねます。当サービスの<span class="marker14">マーケティング、WEBサイト制作、採用コンサルティング</span>など、
								全てがレジャリスくんのおかげで成り立っています。そんなレジャリスくんをどうしても！という企業様がもしいらっしゃれば、<span class="marker14">こっそりご相談ください。</span></td>
						</tr>
					</table><br><br>

				<div align="center">
					<a href="#form">
					<div class="tensyoku-btn">
						資料を請求する 〉
					</div>
					</a>
	         </div>	<br><br>


			<?php endwhile; ?>
		<?php endif; ?>
	</div>
							
	<div class="side-col">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>


<?php
get_footer();
