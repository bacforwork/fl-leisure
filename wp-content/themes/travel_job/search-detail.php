<?php global $wp_query;
		$total_results = $wp_query->found_posts;
    if(isset($_GET['get_ajax']) && $_GET['get_ajax']==1){
      echo $total_results;
      die;
    }?>
<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package travel_job
 */

get_header();
?>

 <div class="main-wid">
	<?php $search_query = get_search_query();?>
	<div class="pan">
		<a href="<?php bloginfo('url'); ?>">TOP</a> > 検索結果
	</div>

	<div class="side-section">
	  <div class="side-col-recDetail"><!-- side-col-recDetail -->
      <?php get_sidebar(2); ?>
      </div>
	  <div class="sideSearchBox">
        <form method="get" id="searchform" action="<?php bloginfo('url'); ?>">
          <div>
            <input type="text" name="s" id="s" value="<?php the_search_query(); ?>"  placeholder="キーワードで検索する" />
          </div>
          <div class="text-searchbtn">
            <input type="hidden" name="post_type" value="detail">
            <input type="submit" class="text-searchbtn" value="検索" />
          </div>
        </form>
      </div>
	</div>

	<div class="main-col-recDetail sp-pad">
		<h1 class="main-col-Ttl">検索結果</h1>

		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ) : the_post(); ?>
			  <div class="recList">
                <ul>
				  <li>
                    <h3>
					  <a href="<?php the_permalink(); ?>">
						<?php the_title(); ?><?php echo post_custom('company'); ?>
					  </a>
					</h3>
                    <div class="recDetail">
                      <a href="<?php the_permalink(); ?>">
						<?php if( get_field('main-img') ): ?>
                          <figure><img src="<?php the_field('main-img'); ?>" width="282" height="212" alt=""/></figure>
                        <?php endif; ?>
					  </a>
                      <div class="txt">
                        <dl>
                          <dt class="place"><span>勤務地</span></dt>
                          <dd><?php echo post_custom('address1'); ?><?php echo post_custom('address2'); ?></dd>
                        </dl>
                        <dl>
                          <dt class="access"><span>アクセス</span></dt>
                          <dd><?php echo nl2br(get_post_meta($post->ID, 'access', true)); ?></dd>
                        </dl>
                        <dl>
                          <dt class="type"><span>雇用形態</span></dt>
                          <dd><?php echo post_custom('employee'); ?></dd>
                        </dl>
                        <dl>
                          <dt class="salary"><span>給与</span></dt>
                          <dd><?php echo post_custom('salarytype'); ?><?php echo post_custom('salary01'); ?>円～<?php if( get_field('salary02') ): ?><?php echo post_custom('salary02'); ?>円<?php endif; ?></dd>
                        </dl>
                        <dl>
                          <dt class="role"><span>募集職種</span></dt>
                          <dd><?php echo post_custom('jobtype'); ?></dd>
                        </dl>
                        <dl>
                          <dt class="particular"><span>こだわり</span></dt>
                          <dd>
                          <?php $terms = get_the_terms( get_the_ID(), 'condition' ); ?>
                            <?php if(!empty($terms)): ?>
                              <?php for($i=0; $i<8; $i++): ?>
                                <?php if(isset($terms[$i])): ?>
                                  <?php echo $terms[$i]->name; ?>｜
                                  <?php else: break;?>
                                <?php endif; ?>
                              <?php endfor; ?>
                            <?php endif; ?>
                          </dd>
                        </dl>
                      </div>
                    </div>
					<div class="comment">
                      <div class="tx">
                        <div class="appealTxt">
                          <?php echo post_custom('maincatch'); ?>
                        </div>
                      </div>
                      <div class="btns">
                        <div class="keepBtn">
						  <a class="keeplist keeping" href="javascript:void(0);" data-jobid="<?php echo get_the_ID(); ?>">キープする</a>
						</div>
                        <a href="<?php the_permalink(); ?>">
                          <div class="detailBtn">詳しく見る</div>
                        </a>
                      </div>
                    </div>
                  </li>
				</ul>
              </div>
			<?php endwhile; ?>

			<div class="pager">
				<?php global $wp_rewrite; $paginate_base = get_pagenum_link(1); if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
					$paginate_format = '';
					$paginate_base = add_query_arg('paged','%#%');
				}
				else{
					$paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
					user_trailingslashit('page/%#%/','paged');;
					$paginate_base .= '%_%';
				}
				echo paginate_links(array(
					'base' => $paginate_base,
					'format' => $paginate_format,
					'total' => $wp_query->max_num_pages,
					'mid_size' => 5,
					'current' => ($paged ? $paged : 1),
					'prev_text' => '«',
					'next_text' => '»',
				)); ?>
			</div>
		<?php else: ?>
			現在、ご指定の条件での求人はありません。
		<?php endif; ?>
	</div>
</div>
<?php
get_footer();