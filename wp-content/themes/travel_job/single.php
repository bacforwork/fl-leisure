<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package travel_job
 */

get_header();
?>

<div class="main-wid">
	<div class="pan">
	<a href="<?php bloginfo('url'); ?>">TOP</a> > <?php the_title(); ?>
	</div>
	<div class="main-col sp-pad marb4">
		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="detail-tit">
					<h1><?php the_title(); ?></h1>
				</div>
				<?php the_content(); ?>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<div class="side-col">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>
<?php
get_footer();