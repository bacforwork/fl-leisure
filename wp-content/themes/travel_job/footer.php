<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package travel_job
 */

?>

	</div><!-- #content -->


<footer>
	<div class="site-footer">
		<div class="main-wid">
			<div class="foot-navi">
				<a href="<?php bloginfo('url'); ?>">TOP</a><br>
				<a href="<?php bloginfo('url'); ?>/attend/">転職アテンドサービス</a><br>
			</div>
			<div class="foot-navi">
				<a href="<?php bloginfo('url'); ?>/detail/">求人情報</a><br>
				<a href="<?php bloginfo('url'); ?>/company/">求人会社情報</a><br>
				<a href="<?php bloginfo('url'); ?>/feature/">インタビュー</a><br>
				<a href="<?php bloginfo('url'); ?>/column/">お役立ちコラム</a>
			</div>
			<div class="foot-navi">
				<a href="<?php bloginfo('url'); ?>/service/">採用をお考えの企業様へ</a><br>
				<a href="<?php bloginfo('url'); ?>/privacy/">個人情報の取り扱いについて</a><br>
				<a href="<?php bloginfo('url'); ?>/kiyaku/">ご利用規約</a><br>
				<a href="<?php bloginfo('url'); ?>/about/">運営会社</a><br>
				<a href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a>
			</div>
			<div class="clear"></div>
		</div>
	</div>

	<?php if (is_mobile()) : ?>
		<div class="site-info">
			Copyright &copy; <?php bloginfo(); ?> All Rights Reserved.
		</div><!-- .site-info -->
	<?php else: ?>
		<div class="site-info">
			<div class="main-wid">
				Copyright &copy; <?php bloginfo(); ?> All Rights Reserved.
			</div>
		</div><!-- .site-info -->
	<?php endif; ?>

</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
