(function($) {
    var keeplist = localStorage.getItem('keeplist');
    if (keeplist) {
        keeplist = JSON.parse(keeplist);
    } else {
        keeplist = [];
    }

    var jobId = null;
    $(function() {
        keeplist.forEach(function (v) {
            $('a.keeplist.keeping[data-jobid="'+v+'"]').text('キープ中').toggleClass('keeping-img').removeClass('nokeep-img');
        })

        $('span.keeplist-count').text(keeplist.length);
        $(document).on('click', 'a.keeplist.keeping', function() {
            jobId = $(this).data('jobid');
            if (keeplist.includes(jobId)) {
                var i = keeplist.indexOf(jobId);
                keeplist.splice(i, 1);
                $('a.keeplist[data-jobid="'+jobId+'"]').text('キープ').toggleClass('nokeep-img').removeClass('keeping-img');
            } else {
                keeplist.push(jobId);
                $('a.keeplist.keeping[data-jobid="'+jobId+'"]').text('キープ中').toggleClass('keeping-img').removeClass('nokeep-img');
            }
            $('span.keeplist-count').text(keeplist.length);
            localStorage.setItem('keeplist', JSON.stringify(keeplist));
        })
    })
})(jQuery);

(function($) {

})(jQuery);
