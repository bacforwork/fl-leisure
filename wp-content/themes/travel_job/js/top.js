// JavaScript Document


window.addEventListener('DOMContentLoaded', function(){
	$('.slide01').infiniteslide({
	'speed': 24, //速さ 単位はpx/秒です。
	'direction': 'left', //up/down/left/rightから選択
	'pauseonhover': false, //マウスオーバーでストップ
	'responsive': false, //子要素の幅を%で指定しているとき
	'clone': 2 //子要素の複製回数
	});
});

window.addEventListener('DOMContentLoaded', function(){
	$('.slide02').infiniteslide({
		'speed': 40, //速さ 単位はpx/秒です。
		'direction': 'left', //up/down/left/rightから選択
		'pauseonhover': false, //マウスオーバーでストップ
		'responsive': false, //子要素の幅を%で指定しているとき
		'clone': 2 //子要素の複製回数
	});
});


$(function($){
        // 「はい」ボタンが押された後の処理
        $(document).on('confirmation', '.remodal', function () {
      alert('サンプル表示です。');
    });
        // モーダルウインドウ表示
    $("#btn_area").click(function(){
    $('[data-remodal-id=modal]').remodal().open();
    });
});

