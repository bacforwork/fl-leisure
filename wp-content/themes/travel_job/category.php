<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package travel_job
 */

get_header();
?>

<div class="main-wid">
	<div class="pan">
		<a href="<?php bloginfo('url'); ?>">TOP</a> > <?php $cat_info = get_category( $cat ); ?><?php echo esc_html( $cat_info->name ); ?>
	</div>
	<div class="main-col sp-pad marb4 ">
		<div class="ar-tit marb2">
			<?php $cat_info = get_category( $cat ); ?><?php echo esc_html( $cat_info->name ); ?>
		</div>
		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<div class="ar-tokusyu-box">
					<div class="ar-tokuyu-img">
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium'); ?></a>
					</div>
					<div class="ar-tokuyu-text">
						<div>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</div>
						<span class="ar-tokuyu-date"><?php the_time('Y年n月j日'); ?></span><br>
						<?php echo mb_substr(strip_tags($post-> post_content),0,100) . '...'; ?><a href="<?php the_permalink(); ?>">続きを読む</a>
					</div>
					<div class="clear"></div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
		<div class="clear marb2"></div>
		<div class="pager">
			<?php global $wp_rewrite; $paginate_base = get_pagenum_link(1); if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
				$paginate_format = '';
				$paginate_base = add_query_arg('paged','%#%');
			}
			else{
				$paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
				user_trailingslashit('page/%#%/','paged');;
				$paginate_base .= '%_%';
			}
			echo paginate_links(array(
				'base' => $paginate_base,
				'format' => $paginate_format,
				'total' => $wp_query->max_num_pages,
				'mid_size' => 5,
				'current' => ($paged ? $paged : 1),
				'prev_text' => '«',
				'next_text' => '»',
			)); ?>
		</div>
	</div>
								
	<div class="side-col">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>


<?php
get_footer();