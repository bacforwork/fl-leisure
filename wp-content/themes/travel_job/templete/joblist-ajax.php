<div class="ar-workbox marb4">
   <h3><?php the_title(); ?><?php echo post_custom('company'); ?></h3>
                    <div class="recDetail">
                      <?php if( get_field('main-img') ): ?>
                        <figure><img src="<?php the_field('main-img'); ?>" width="282" height="212" alt=""/></figure>
                      <?php endif; ?>
                      <div class="txt">
                        <dl>
                          <dt class="place"><span>勤務地</span></dt>
                          <dd><?php echo post_custom('address1'); ?><?php echo post_custom('address2'); ?><?php echo post_custom('address3'); ?></dd>
                        </dl>
                        <dl>
                          <dt class="access"><span>アクセス</span></dt>
                          <dd><?php echo nl2br(get_post_meta($post->ID, 'access', true)); ?></dd>
                        </dl>
                        <dl>
                          <dt class="type"><span>雇用形態</span></dt>
                          <dd><?php echo post_custom('employee'); ?></dd>
                        </dl>
                        <dl>
                          <dt class="salary"><span>給与</span></dt>
                          <dd><?php echo post_custom('salarytype'); ?><?php echo post_custom('salary01'); ?>円～<?php if( get_field('salary02') ): ?><?php echo post_custom('salary02'); ?>円<?php endif; ?></dd>
                        </dl>
                        <dl>
                          <dt class="role"><span>募集職種</span></dt>
                          <dd><?php echo post_custom('jobtype'); ?></dd>
                        </dl>
                        <dl>
                          <dt class="particular"><span>こだわり</span></dt>
                          <dd>
                          <?php $terms = get_the_terms( get_the_ID(), 'condition' ); ?>
                            <?php if(!empty($terms)): ?>
                              <?php for($i=0; $i<8; $i++): ?>
                                <?php if(isset($terms[$i])): ?>
                                  <?php echo $terms[$i]->name; ?>｜
                                  <?php else: break;?>
                                <?php endif; ?>
                              <?php endfor; ?>
                            <?php endif; ?>
                          </dd>
                        </dl>
                      </div>
                    </div>
					<div class="comment">
                      <div class="tx">
                        <div class="appealTxt">
                          <?php echo post_custom('maincatch'); ?>
                        </div>
                      </div>
                      <div class="btns">
                        <div class="keepBtn">
						  <a class="keeplist keeping" href="javascript:void(0);" data-jobid="<?php echo get_the_ID(); ?>">キープする</a>
						</div>
                        <a href="<?php the_permalink(); ?>">
                          <div class="detailBtn">詳しく見る</div>
                        </a>
                      </div>
                    </div>
</div>
