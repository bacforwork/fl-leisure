<form method="get" id="searchform" action="<?php bloginfo('url'); ?>">
    <div class="text-search">
        <input type="text" name="s" id="s" value="<?php the_search_query(); ?>"  placeholder="キーワードを入力してください" />
    </div>
    <div class="text-searchbtn">
        <input type="hidden" name="post_type" value="detail">
        <input type="submit" class="text-searchbtn" value="検索" />
    </div>
    <div class="clear"></div>
</form>