<div class="top-search">
    <form method="get" id="searchform" action="<?php bloginfo('url'); ?>">
        <?php
            $taxonomy = 'area';
            $parents = get_terms( $taxonomy, ['pad_counts' => true, 'hide_empty' => false, 'parent' => 0 ] );
        ?>
        <div class="selectbox">
            <select name="area" class="selectbox">
            <option value="" selected="true">地域を選択</option>
                <?php foreach ( $parents as $parent ) :?>
                    <option value="<?php echo $parent->slug; ?>"><?php echo $parent->name; ?></option>
                    <?php $children = get_term_children( $parent->term_id, $taxonomy ); ?>
                    <?php foreach ( $children as $child ):?>
                        <?php $termC = get_term_by( 'id' , $child, $taxonomy ); ?>
                        <?php if( $termC->count != 0 ): ?>
                        <option value="<?php echo $termC->slug; ?>"><?php echo '　└ '.$termC->name; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </select>
        </div>

        <?php
        $taxonomy = 'jobtype';
        $parents = get_terms( $taxonomy, ['pad_counts' => true, 'hide_empty' => false, 'parent' => 0 ] );
        ?>
        <div class="selectbox2">
            <select name="jobtype">
                <option value="" selected="true">職種を選択</option>
                <?php foreach ( $parents as $parent ) :?>
                    <option value="<?php echo $parent->slug; ?>"><?php echo $parent->name; ?></option>
                    <?php $children = get_term_children( $parent->term_id, $taxonomy ); ?>
                    <?php foreach ( $children as $child ):?>
                        <?php $termC = get_term_by( 'id' , $child, $taxonomy ); ?>
                        <?php if( $termC->count != 0 ): ?>
                        <option value="<?php echo $termC->slug; ?>"><?php echo '　└ '.$termC->name; ?></option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="selectbox">
            <select name="employee">
            <option value="" selected="true">雇用形態を選択</option>
            <?php
            $taxonomy_name = 'employee';
            $taxonomys = get_terms($taxonomy_name);
            if(!is_wp_error($taxonomys) && count($taxonomys)):
                foreach($taxonomys as $taxonomy):
                    $tax_posts = get_posts(array('post_type' => get_post_type(detail), 'taxonomy' => $taxonomy_name, 'term' => $taxonomy->slug ) );
                    if($tax_posts):
            ?>
                <option value="<?php echo $taxonomy->slug; ?>"><?php echo $taxonomy->name; ?></option>
            <?php endif;endforeach;endif;?>
            </select>
        </div>
        <div class="clear marb2"></div>
        <input type="hidden" name="s">
        <div align="center">
            <input type="hidden" name="post_type" value="detail">
            <span><input type="submit" value="検索"/></span>
        </div>
    </form>
</div>