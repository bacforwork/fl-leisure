<div class="searchBox">
  <form method="get" id="searchform" action="<?php bloginfo('url'); ?>">
    <div class="searchBoxInr">
      <?php
      $taxonomy = 'area';
      $parents = get_terms($taxonomy, ['pad_counts' => true, 'hide_empty' => false, 'parent' => 0]);
      ?>
      <div class="btnSearchItems">
        <div class="searchIcon">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/iconPlace.png" alt="勤務地">
        </div>
        <div class="btnBottom">
          <p class="btnBottomTxt">
            <select name="area">
              <option value="" selected="true">エリアを選ぶ ▼</option>
              <?php foreach ($parents as $parent) : ?>
                <option value="<?php echo $parent->slug; ?>"><?php echo $parent->name; ?></option>
                <?php $children = get_term_children($parent->term_id, $taxonomy); ?>
                <?php foreach ($children as $child) : ?>
                  <?php $termC = get_term_by('id', $child, $taxonomy); ?>
                  <?php if ($termC->count != 0) : ?>
                    <option value="<?php echo $termC->slug; ?>"><?php echo '　└ ' . $termC->name; ?></option>
                  <?php endif; ?>
                <?php endforeach; ?>
              <?php endforeach; ?>
            </select>
          </p>
        </div>
      </div>
      <?php
      $taxonomy = 'jobtype';
      $parents = get_terms($taxonomy, ['pad_counts' => true, 'hide_empty' => false, 'parent' => 0]);
      ?>
      <div class="btnSearchItems">
        <div class="searchIcon">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/iconOccupation.png" alt="職種">
        </div>
        <div class="btnBottom">
          <p class="btnBottomTxt">
            <select name="jobtype">
              <option value="" selected="true">職種を選ぶ ▼</option>
              <?php foreach ($parents as $parent) : ?>
                <option value="<?php echo $parent->slug; ?>"><?php echo $parent->name; ?></option>
                <?php $children = get_term_children($parent->term_id, $taxonomy); ?>
                <?php foreach ($children as $child) : ?>
                  <?php $termC = get_term_by('id', $child, $taxonomy); ?>
                  <?php if ($termC->count != 0) : ?>
                    <option value="<?php echo $termC->slug; ?>"><?php echo '　└ ' . $termC->name; ?></option>
                  <?php endif; ?>
                <?php endforeach; ?>
              <?php endforeach; ?>
            </select>
          </p>
        </div>
      </div>

      <div class="btnSearchItems">
        <div class="searchIcon">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/common/iconStatus.png" alt="雇用形態">
        </div>
        <div class="btnBottom">
          <p class="btnBottomTxt">
            <select name="employee">
              <option value="" selected="true">雇用形態を選ぶ ▼</option>
              <?php
              $taxonomy_name = 'employee';
              $taxonomys = get_terms($taxonomy_name);
              if (!is_wp_error($taxonomys) && count($taxonomys)) :
                foreach ($taxonomys as $taxonomy) :
                  $tax_posts = get_posts(array('post_type' => get_post_type(detail), 'taxonomy' => $taxonomy_name, 'term' => $taxonomy->slug));
                  if ($tax_posts) :
              ?>
              <option value="<?php echo $taxonomy->slug; ?>"><?php echo $taxonomy->name; ?></option>
              <?php endif;
                endforeach;
              endif; ?>
            </select>
          </p>
        </div>
      </div>
    </div>
    <input type="hidden" name="s">
    <input type="hidden" name="post_type" value="detail">
    <button class="searchBoxSubmit" type="submit">
      <div class="searchBoxSubmitInr">
        <span class="searchBoxSubmitTxt">検索する</span>
        <span class="searchBoxSubmitIcon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/iconSearch.png"></span>
      </div>
    </button>
  </form>
</div>