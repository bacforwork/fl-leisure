<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package travel_job
 */

get_header();
?>

<div class="main-wid">
	<div class="pan">
		<a href="<?php bloginfo('url'); ?>">TOP</a> > 会社情報一覧</a>
	</div>
	<div class="main-col sp-pad marb4 ">
		<div class="ar-tit marb2">
			会社情報一覧
		</div>

		<div class="ar-company-tit">
			あ行の会社名一覧
		</div>
		<!--あ-->
		<div class="kana">
			あ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'a',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--い-->
		<div class="kana">
			い
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'i',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--う-->
		<div class="kana">
			う
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'u',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--え-->
		<div class="kana">
			え
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'e',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--お-->
		<div class="kana">
			お
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'o',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<div class="ar-company-tit mart4">
			か行の会社名一覧
		</div>
		<!--か-->
		<div class="kana">
			か
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ka',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--き-->
		<div class="kana">
			き
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ki',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--く-->
		<div class="kana">
			く
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ku',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--け-->
		<div class="kana">
			け
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ke',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--こ-->
		<div class="kana">
			こ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ko',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<div class="ar-company-tit mart4">
			さ行の会社名一覧
		</div>
		<!--さ-->
		<div class="kana">
			さ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'sa',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--し-->
		<div class="kana">
			し
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'si',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--す-->
		<div class="kana">
			す
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'su',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--せ-->
		<div class="kana">
			せ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'se',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--そ-->
		<div class="kana">
			そ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'so',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<div class="ar-company-tit mart4">
			た行の会社名一覧
		</div>
		<!--た-->
		<div class="kana">
			た
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ta',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--ち-->
		<div class="kana">
			ち
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ti',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--つ-->
		<div class="kana">
			つ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'tu',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--て-->
		<div class="kana">
			て
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'te',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--と-->
		<div class="kana">
			と
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'to',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<div class="ar-company-tit mart4">
			な行の会社名一覧
		</div>
		<!--な-->
		<div class="kana">
			な
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'na',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--に-->
		<div class="kana">
			に
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ni',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--ぬ-->
		<div class="kana">
			ぬ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'nu',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--ね-->
		<div class="kana">
			ね
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ne',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--の-->
		<div class="kana">
			の
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'no',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>


		<div class="ar-company-tit mart4">
			は行の会社名一覧
		</div>
		<!--は-->
		<div class="kana">
			は
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ha',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--ひ-->
		<div class="kana">
			ひ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'hi',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--ふ-->
		<div class="kana">
			ふ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'hu',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--へ-->
		<div class="kana">
			へ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'he',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--ほ-->
		<div class="kana">
			ほ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ho',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>


		<div class="ar-company-tit mart4">
			ま行の会社名一覧
		</div>
		<!--ま-->
		<div class="kana">
			ま
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ma',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--み-->
		<div class="kana">
			み
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'mi',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--む-->
		<div class="kana">
			む
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'mu',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--め-->
		<div class="kana">
			め
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'me',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--も-->
		<div class="kana">
			も
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'mo',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<div class="ar-company-tit mart4">
			や行の会社名一覧
		</div>
		<!--や-->
		<div class="kana">
			や
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ya',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--ゆ-->
		<div class="kana">
			ゆ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'yu',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--よ-->
		<div class="kana">
			よ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'yo',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>


		<div class="ar-company-tit mart4">
			ら行の会社名一覧
		</div>
		<!--ら-->
		<div class="kana">
			ら
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ra',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--り-->
		<div class="kana">
			り
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ri',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--る-->
		<div class="kana">
			る
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ru',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--れ-->
		<div class="kana">
			れ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 're',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--ろ-->
		<div class="kana">
			ろ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'ro',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>


		<div class="ar-company-tit mart4">
			わ行の会社名一覧
		</div>
		<!--わ-->
		<div class="kana">
			わ
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'wa',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--を-->
		<div class="kana">
			を
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'wo',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>

		<!--ん-->
		<div class="kana">
			ん
		</div><br>
		<?php
			$term = array_shift(get_the_terms($post->ID, 'companyname'));
			$args = array(
				'post_type'=>'company',
				'posts_per_page'=>-1,
				'meta_key' => 'companyname_furigana',
				'orderby' => 'meta_value',
				'order' => 'ASC',
				'tax_query'=>array(
					array(
						'taxonomy'=>'kana',
						'field'=>'slug',
						'terms' => 'nn',
						),
					),
				);
			$posts = get_posts( $args );
			if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
				<div class="kana-company">
					<a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a>
				</div>
			<?php endforeach; ?>
			<?php else: ?>
				現在募集している企業はありません。
		<?php endif; wp_reset_postdata(); ?><br>


	</div>
								
	<div class="side-col">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>


<?php
get_footer();