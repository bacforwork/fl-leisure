<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package moumou
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<!-- 閲覧回数ランキング -->
  <h2 class="viewRankTitle">人気求人ランキング</h2>
  <div class="viewRank">
    <ul>
    <?php $args = array(
	'post_type'=>'detail',
	'posts_per_page'=>6,
	'meta_key'=>'popular_posts',
	'orderby'=>'date',
	'order'=>'DESC',
	);
	$posts = get_posts( $args );
	if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
      <li>
        <div class="counter"></div>
          <a href="<?php the_permalink(); ?>">
            <div class="heading">
			<?php
				$image_id = get_post_thumbnail_id();
				$image_url = wp_get_attachment_image_src($image_id, true);
			?>
            <figure><img src="<?php echo $image_url[0]; ?>)" alt=""/></figure>
            <div class="tit"><?php the_title(); ?></div>
            </div>
            <dl>
              <dt><span>勤務地</span></dt>
              <dd><?php echo post_custom('address1'); ?><?php echo post_custom('address2'); ?></dd>
            </dl>
            <dl>
              <dt><span>募集職種</span></dt>
              <dd>
			    <?php
				if ($terms = get_the_terms($post->ID, 'jobtype')) {
					foreach ( $terms as $term ) {
						echo esc_html($term->name)  ;
					}
				}
			    ?>
			  </dd>
            </dl>
          </a>
      </li>
	  <?php endforeach; ?>
    <?php endif; wp_reset_postdata(); ?>
	</ul>
  </div>
  
  <?php if (is_mobile()) : ?>
  <?php else: ?>	
<!-- インタビューランキング -->
  <h2 class="interviewRankTitle">インタビューランキング</h2>
  <div class="interviewRank">
　   <?php $popular = new WP_Query(array(
		'post_type'=>'feature',
		'posts_per_page'=>5,
		'meta_key'=>'popular_posts',
		'orderby'=>'meta_value_num',
		'order'=>'DESC'
		));
		while ($popular->have_posts()) : $popular->the_post(); ?>
	<ul>
      <li>
        <a href="<?php the_permalink(); ?>">
          <figure>
            <?php the_post_thumbnail(); ?>
          </figure>
		  <div class="tie">
            <div class="name"><?php echo post_custom('gyosyu'); ?></div>
          </div>
          <div class="tit">
          <?php the_title(); ?>
          </div>
        </a>
      </li>
　   
	<?php endwhile; wp_reset_postdata(); ?>
<?php endif; ?>
		</ul>
  </div>