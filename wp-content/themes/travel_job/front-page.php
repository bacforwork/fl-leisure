<?php
/**
 * For frontpage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package travel_job
 */

get_header();
?>

<!--  消した  -->

      <section class="fv">
		<div class="fvInner">
          <div class="fvTitle">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/title.png" alt="業界初！レジャー関連専門求人転職サイト" class="pc">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/title_sp.png" alt="業界初！レジャー関連専門求人転職サイト" class="sp">
          </div>
			<?php get_search_form(); ?>
		</div>
        <div class="topSlider">
		  <div class="fvGradient"></div>
		  <div class="slide01">
            <ul>
              <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/slide01/slider01_img_01.jpg" alt="" /></li>
              <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/slide01/slider01_img_02.jpg" alt="" /></li>
			  <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/slide01/slider01_img_03.jpg" alt="" /></li>
              <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/slide01/slider01_img_04.jpg" alt="" /></li>
			  <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/slide01/slider01_img_05.jpg" alt="" /></li>
              <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/slide01/slider01_img_06.jpg" alt="" /></li>
            </ul>
          </div>
		  <div class="slide02">
            <ul>
              <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/slide02/slider02_img_01.jpg" alt="" /></li>
              <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/slide02/slider02_img_02.jpg" alt="" /></li>
			  <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/slide02/slider02_img_03.jpg" alt="" /></li>
              <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/slide02/slider02_img_04.jpg" alt="" /></li>
			  <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/slide02/slider02_img_05.jpg" alt="" /></li>
              <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/slide02/slider02_img_06.jpg" alt="" /></li>
            </ul>
          </div>
		</div>
		<div class="fv-subIllust">
		  <div class="fv-star01">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/fv_star01.png">
		  </div>
		  <div class="fv-star02">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/fv_star02.png">
		  </div>
		  <div class="fv-airplane">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/fv_airplane.png">
		  </div>
		</div>
      </section> 
<!-- fv -->
<!-- popular テンプレ化必要-->		
	  <section class="popular">
	    <div class="popularInner">
          <div class="sectionTitle balloon1">
            <h2>人気の求人情報</h2>
          </div>
		  <div class="popularCardlist">
            <?php $args = array(
	        'post_type'=>'detail',
	        'posts_per_page'=>9,
	        'orderby'=>'date',
	        'order'=>'DESC',
	        'tax_query'=>array(
	            array(
	                'taxonomy'=>'option',
	                'field'=>'slug',
	                'terms'=>'topbana',
	                ),
	            ),
	        );
	        $posts = get_posts( $args );
	        if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
	        <div class="popularCard">
              <a href="<?php the_permalink(); ?>">
			    <?php
                $image_id = get_post_thumbnail_id();
                $image_url = wp_get_attachment_image_src($image_id, true);
                ?>
                <div class="popularCard_title">
                  <h3><?php the_title(); ?></h3>
                </div>
                <div class="popularCard_inner">
                  <div class="popularCard_inner_photo">
                    <figure><img src="<?php echo $image_url[0]; ?>" width="100%" alt=""/></figure>
                  </div>
                  <div class="popularCard_inner_info">
                    <ul class="popularCard_inner_place">
                      <li class="item pc">勤務地</li>
                      <li><?php echo post_custom('address1'); ?><?php echo post_custom('address2'); ?></li>
                    </ul>
                    <ul class="popularCard_inner_occupation">
                      <li class="item pc">募集職種</li>
                      <li><?php
	                    if ($terms = get_the_terms($post->ID, 'jobtype')) {
	                        foreach ( $terms as $term ) {
	                            echo esc_html($term->name)  ;
	                        }
	                    }
	                ?></li>
                    </ul>
					<ul>
					  <li class="item pc">給与</li>
					  <li><?php echo post_custom('salarytype'); ?><?php echo post_custom('salary01'); ?>円～<?php if( get_field('salary02') ): ?><?php echo post_custom('salary02'); ?>円<?php endif; ?>
					  </li>
					</ul>
                  </div>
                </div>
                <div class="popularCard_inner_tag">
                  <span>
				    <?php $terms = get_the_terms( get_the_ID(), 'condition' ); ?>
                      <?php if(!empty($terms)): ?>
                        <?php for($i=0; $i<8; $i++): ?>
                          <?php if(isset($terms[$i])): ?>
                            <?php echo $terms[$i]->name; ?>｜
                            <?php else: break;?>
                          <?php endif; ?>
                        <?php endfor; ?>
                      <?php endif; ?>
				  </span>
                </div>
              </a>
            </div>
			<?php endforeach; ?>
    	    <?php endif; wp_reset_postdata(); ?>
		  </div>
		  <script>
            $(function() {
                $('.popularCardlist').slick({
                    infinite: true,
                    dots:false,
                    slidesToShow: 3,
                    slidesToScroll: 1,
					autoplay:  true,
					pauseOnHover: true,
					responsive: [
                    {
                        breakpoint: 768, //767px以下のサイズに適用
                        settings: {
                        slidesToShow:2,
						centerMode: true,
						centerPadding: '20px'	
                        }
                    }
                    ]
                });
            });
        </script>
		</div>
	  </section>
      
      <!-- 2カラムここから -->
      <div class="main-wid">
        <div class="main-col">
          <section class="topMainBanner">
            <div class="">
              <ul>
                <li class="pc">
                  <a href="/attend"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/bnr_attend.png" alt="「転職したい人も、求める企業も。トータルでサポートいたします！」LEISURist転職サポート" /></a>
                </li>
				<li class="sp">
                  <a href="/attend"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/bnr_attend_sp.png" alt="「転職したい人も、求める企業も。トータルでサポートいたします！」LEISURist転職サポート" /></a>
                </li>
              </ul>
            </div>
          </section>
          <section class="topSearchOccupation">
            <div class="topMain_SectionTitle">
              <h2>業種・職種から探す</h2>
              <div class="topBtn_goIndex pc">
                <a href="/company">求人情報一覧へ</a>
              </div>
            </div>
			<div class="topMain_SectionInr">
			  <ul>
                <li class="tMSa">
				  <div class="topMain_SectionInr_agent">
                    <div class="topMain_company_head_agent">旅行会社</div>
                    <span class="topMain_company_body pc">
                      <a href="<?php bloginfo('url'); ?>/jobtype/agent/sales/">営業・セールス</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/agent/conductor/">添乗員・コンダクター</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/agent/guide/">ガイド・通訳</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/agent/web/">web担当・マーケター</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/agent/officework/">手配事務・経理</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/agent/planner/">企画・プランナー</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/agent/driver/">バスドライバー</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/agent/writer/">ライター・クリエイター</a>
                    </span>
					<div class="sp topMain_company_body_sp">
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/agent/sales/">営業・セールス</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/agent/conductor/">添乗員・コンダクター</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/agent/guide/">ガイド・通訳</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/agent/web/">web担当・マーケター</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/agent/officework/">手配事務・経理</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/agent/planner/">企画・プランナー</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/agent/driver/">バスドライバー</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/agent/writer/">ライター・クリエイター</a>
					  </div>
					</div>
				  </div>
                </li>
				<li class="tMSo">
 				  <div class="topMain_SectionInr_online">
                    <div class="topMain_company_head_online">オンライン/OTA</div>
                    <span class="topMain_company_body pc">
                      <a href="<?php bloginfo('url'); ?>/jobtype/online/sales-online/">営業・セールス</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/online/engineer/">エンジニア</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/online/marketing/">マーケティング</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/online/office-work/">手配事務・経理</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/online/planner-online/">企画・プランナー</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/online/designer/">デザイナー</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/online/director/">ディレクター</a>
                    </span>
					<div class="sp topMain_company_body_sp">
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/online/sales-online/">営業・セールス</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/online/engineer/">エンジニア</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/online/marketing/">マーケティング</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/online/office-work/">手配事務・経理</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/online/planner-online/">企画・プランナー</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/online/designer/">デザイナー</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/online/director/">ディレクター</a>
					  </div>
					</div>
				  </div>
                </li>
				<li class="tMSh">
				  <div class="topMain_SectionInr_hotel">
                    <div class="topMain_company_head_hotel">ホテル</div>
                    <span class="topMain_company_body pc">
                      <a href="<?php bloginfo('url'); ?>/jobtype/hotel/front/">フロント</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/hotel/manager/">マネージャー</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/hotel/reception/">接客係</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/hotel/sales-hotel/">営業・セールス</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/hotel/officework-hotel/">予約受付・事務</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/hotel/restaurant/">レストラン</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/hotel/cock/">調理スタッフ</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/hotel/kitchen/">洗い場スタッフ</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/hotel/cleaning/">清掃スタッフ</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/hotel/fother/">その他</a>
                    </span>
					<div class="sp topMain_company_body_sp">
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/hotel/front/">フロント</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/hotel/manager/">マネージャー</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/hotel/reception/">接客係</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/hotel/sales-hotel/">営業・セールス</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/hotel/officework-hotel/">予約受付・事務</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/hotel/restaurant/">レストラン</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/hotel/cock/">調理スタッフ</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/hotel/kitchen/">洗い場スタッフ</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/hotel/cleaning/">清掃スタッフ</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/hotel/fother/">その他</a>
					  </div>
					</div>
				  </div>
                </li>
				<li class="tMSr">
				  <div class="topMain_SectionInr_ryokan">
                    <div class="topMain_company_head_ryokan">旅館</div>
                    <span class="topMain_company_body pc">
                      <a href="<?php bloginfo('url'); ?>/jobtype/ryokan/front-ryokan/">フロント</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/ryokan/manager-ryokan/">支配人（幹部候補）</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/ryokan/ryokan/nakai/">仲居（客室係）</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/ryokan/sales-ryokan/">営業・セールス</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/ryokan/officework-ryokan/">予約受付・事務</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/ryokan/restaurant-ryokan/">レストラン</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/ryokan/cock-ryokan/">調理スタッフ</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/ryokan/kitchen-ryokan/">洗い場スタッフ</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/ryokan/cleaning-ryokan/">清掃スタッフ</a>｜
                      <a href="<?php bloginfo('url'); ?>/jobtype/ryokan/other-ryokan/">その他</a>
                    </span>
					<div class="sp topMain_company_body_sp">
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/ryokan/front-ryokan/">フロント</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/ryokan/manager-ryokan/">支配人（幹部候補）</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/ryokan/ryokan/nakai/">仲居（客室係）</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/ryokan/sales-ryokan/">営業・セールス</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/ryokan/officework-ryokan/">予約受付・事務</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/ryokan/restaurant-ryokan/">レストラン</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/ryokan/cock-ryokan/">調理スタッフ</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/ryokan/kitchen-ryokan/">洗い場スタッフ</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/ryokan/cleaning-ryokan/">清掃スタッフ</a>
					  </div>
					  <div class="topMain_company_body_spList">
						<a href="<?php bloginfo('url'); ?>/jobtype/ryokan/other-ryokan/">その他</a>
					  </div>
					</div>
				  </div>
                </li>
              </ul>
			</div>
          </section>
          <section class="topSearchArea">
            <div class="topMain_SectionTitle">
              <h2>エリアから探す</h2>
              <div class="topBtn_goIndex pc">
                <a href="/company">求人情報一覧へ</a>
              </div>
			</div>
            <div class="topMain_SectionInr">
              <div class="top-map">
                <div class="posi">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/topmap.png" width="100%" alt="日本地図">
                    <div class="posimap posi1">
                        <a href="<?php bloginfo('url'); ?>/area/tohoku/">北海道・東北</a>
                    </div>
                    <div class="posimap posi2">
                        <a href="<?php bloginfo('url'); ?>/area/hokuriku/">北陸・甲信越</a>
                    </div>
                    <div class="posimap posi3">
                        <a href="<?php bloginfo('url'); ?>/area/kanto/">関東</a>
                    </div>
                    <div class="posimap posi4">
                        <a href="<?php bloginfo('url'); ?>/area/tokai/">東海</a>
                    </div>
                    <div class="posimap posi5">
                        <a href="<?php bloginfo('url'); ?>/area/kansai/">関西</a>
                    </div>
                    <div class="posimap posi6">
                        <a href="<?php bloginfo('url'); ?>/area/chugoku/">中国</a>
                    </div>
                    <div class="posimap posi7">
                        <a href="<?php bloginfo('url'); ?>/area/shikoku/">四国</a>
                    </div>
                    <div class="posimap posi8">
                        <a href="<?php bloginfo('url'); ?>/area/kyushu/">九州・沖縄</a>
                    </div>
                </div>
              </div>
              <div class="top-map2 pc">
                  <p><a href="<?php bloginfo('url'); ?>/area/kanto/">関東</a></p>
                  <a href="<?php bloginfo('url'); ?>/area/kanto/tokyo/">東京</a>｜<a href="<?php bloginfo('url'); ?>/area/kanto/kanagawa/">神奈川</a>｜<a href="<?php bloginfo('url'); ?>/area/kanto/saitama/">埼玉</a>｜<a href="<?php bloginfo('url'); ?>/area/kanto/chiba/">千葉</a>｜<a href="<?php bloginfo('url'); ?>/area/kanto/ibaraki/">茨城</a>｜<a href="<?php bloginfo('url'); ?>/area/kanto/tochigi/">栃木</a>｜<a href="<?php bloginfo('url'); ?>/area/kanto/gunma/">群馬</a><br>
                  <p><a href="<?php bloginfo('url'); ?>/area/kansai/">関西</a></p>
                  <a href="<?php bloginfo('url'); ?>/area/kansai/osaka/">大阪</a>｜<a href="<?php bloginfo('url'); ?>/area/kansai/hyogo/">兵庫</a>｜<a href="<?php bloginfo('url'); ?>/area/kansai/kyoto/">京都</a>｜<a href="<?php bloginfo('url'); ?>/area/kansai/shiga/">滋賀</a>｜<a href="<?php bloginfo('url'); ?>/area/kansai/nara/">奈良</a>｜<a href="<?php bloginfo('url'); ?>/area/kansai/wakayama/">和歌山</a><br>
                  <p><a href="<?php bloginfo('url'); ?>/area/tokai/">東海</a></p>
                  <a href="<?php bloginfo('url'); ?>/area/tokai/aichi/">愛知</a>｜<a href="<?php bloginfo('url'); ?>/area/tokai/shizuoka/">静岡</a>｜<a href="<?php bloginfo('url'); ?>/area/tokai/gifu/">岐阜</a>｜<a href="<?php bloginfo('url'); ?>/area/tokai/mie/">三重</a><br>
                  <p><a href="<?php bloginfo('url'); ?>/area/tohoku/">北海道・東北</a></p>
                  <a href="<?php bloginfo('url'); ?>/area/tohoku/hokkaido/">北海道</a>｜<a href="<?php bloginfo('url'); ?>/area/tohoku/miyagi/">宮城</a>｜<a href="<?php bloginfo('url'); ?>/area/tohoku/fukushima/">福島</a>｜<a href="<?php bloginfo('url'); ?>/area/tohoku/aomori/">青森</a>｜<a href="<?php bloginfo('url'); ?>/area/tohoku/iwate/">岩手</a>｜<a href="<?php bloginfo('url'); ?>/area/tohoku/yamagata/">山形</a>｜<a href="<?php bloginfo('url'); ?>/area/tohoku/akita/">秋田</a><br>
                  <p><a href="<?php bloginfo('url'); ?>/area/hokuriku/">北陸・甲信越</a></p>
                  <a href="<?php bloginfo('url'); ?>/area/hokuriku/niigata/">新潟</a>｜<a href="<?php bloginfo('url'); ?>/area/hokuriku/nagano/">長野</a>｜<a href="<?php bloginfo('url'); ?>/area/hokuriku/ishikawa/">石川</a>｜<a href="<?php bloginfo('url'); ?>/area/hokuriku/toyama/">富山</a>｜<a href="<?php bloginfo('url'); ?>/area/hokuriku/yamanashi/">山梨</a>｜<a href="<?php bloginfo('url'); ?>/area/hokuriku/fukui/">福井</a><br>
                  <p><a href="<?php bloginfo('url'); ?>/area/chugoku/">中国</a></p>
                  <a href="<?php bloginfo('url'); ?>/area/chugoku/hiroshima/">広島</a>｜<a href="<?php bloginfo('url'); ?>/area/chugoku/okayama/">岡山</a>｜<a href="<?php bloginfo('url'); ?>/area/chugoku/yamaguchi/">山口</a>｜<a href="<?php bloginfo('url'); ?>/area/chugoku/shimane/">島根</a>｜<a href="<?php bloginfo('url'); ?>/area/chugoku/tottori/">鳥取</a><br>
                  <p><a href="<?php bloginfo('url'); ?>/area/shikoku/">四国</a></p>
                  <a href="<?php bloginfo('url'); ?>/area/shikoku/ehime/">愛媛</a>｜<a href="<?php bloginfo('url'); ?>/area/shikoku/kagawa/">香川</a>｜<a href="<?php bloginfo('url'); ?>/area/shikoku/tokushima/">徳島</a>｜<a href="<?php bloginfo('url'); ?>/area/shikoku/kochi/">高知</a><br>
                  <p><a href="<?php bloginfo('url'); ?>/area/kyushu/">九州・沖縄</a></p>
                  <a href="<?php bloginfo('url'); ?>/area/kyushu/fukuoka/">福岡</a>｜<a href="<?php bloginfo('url'); ?>/area/kyushu/kumamoto/">熊本</a>｜<a href="<?php bloginfo('url'); ?>/area/kyushu/kagoshima/">鹿児島</a>｜<a href="<?php bloginfo('url'); ?>/area/kyushu/nagasaki/">長崎</a>｜<a href="<?php bloginfo('url'); ?>/area/kyushu/oita/">大分</a>｜<a href="<?php bloginfo('url'); ?>/area/kyushu/miyazaki/">宮崎</a>｜<a href="<?php bloginfo('url'); ?>/area/kyushu/saga/">佐賀</a>｜<a href="<?php bloginfo('url'); ?>/area/kyushu/okinawa/">沖縄</a>
              </div>
			  <div class="sp topMain_company_body_sp">
                <div class="topMain_company_body_spListB">
                  <a href="<?php bloginfo('url'); ?>/area/kanto/">関東</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kanto/tokyo/">東京</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kanto/kanagawa/">神奈川</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kanto/saitama/">埼玉</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kanto/chiba/">千葉</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kanto/ibaraki/">茨城</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kanto/tochigi/">栃木</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kanto/gunma/">群馬</a>
                </div>
				<div class="topMain_company_body_spListB">
                  <a href="<?php bloginfo('url'); ?>/area/kansai/">関西</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kansai/osaka/">大阪</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kansai/hyogo/">兵庫</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kansai/kyoto/">京都</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kansai/shiga/">滋賀</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kansai/nara/">奈良</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kansai/wakayama/">和歌山</a>
                </div>
                <div class="topMain_company_body_spListB">
                  <a href="<?php bloginfo('url'); ?>/area/tokai/">東海</a>
                </div>
				<div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/tokai/aichi/">愛知</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/tokai/shizuoka/">静岡</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/tokai/gifu/">岐阜</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/tokai/mie/">三重</a>
                </div>
                <div class="topMain_company_body_spListB">
                  <a href="<?php bloginfo('url'); ?>/area/tohoku/">北海道・東北</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/tohoku/hokkaido/">北海道</a>
                </div>
				<div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/tohoku/miyagi/">宮城</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/tohoku/fukushima/">福島</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/tohoku/aomori/">青森</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/tohoku/iwate/">岩手</a>
                </div>
				<div class="topMain_company_body_spList">
				  <a href="<?php bloginfo('url'); ?>/area/tohoku/yamagata/">山形</a>
                </div>
				<div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/tohoku/akita/">秋田</a>
                </div>
                <div class="topMain_company_body_spListB">
                  <a href="<?php bloginfo('url'); ?>/area/hokuriku/">北陸・甲信越</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/hokuriku/niigata/">新潟</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/hokuriku/nagano/">長野</a>
                </div>
			    <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/hokuriku/ishikawa/">石川</a>
                </div>
				<div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/hokuriku/toyama/">富山</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/hokuriku/yamanashi/">山梨</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/hokuriku/fukui/">福井</a>
                </div>
                <div class="topMain_company_body_spListB">
                  <a href="<?php bloginfo('url'); ?>/area/chugoku/">中国</a>
                </div>
				<div class="topMain_company_body_spList">
				  <a href="<?php bloginfo('url'); ?>/area/chugoku/hiroshima/">広島</a>
                </div>
				<div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/chugoku/okayama/">岡山</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/chugoku/yamaguchi/">山口</a>
                </div>
			    <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/chugoku/shimane/">島根</a>
                </div>
				<div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/chugoku/tottori/">鳥取</a>
                </div>
                <div class="topMain_company_body_spListB">
                  <a href="<?php bloginfo('url'); ?>/area/shikoku/">四国</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/shikoku/ehime/">愛媛</a>
                </div>
				<div class="topMain_company_body_spList">
				  <a href="<?php bloginfo('url'); ?>/area/shikoku/kagawa/">香川</a>
                </div>
				<div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/shikoku/tokushima/">徳島</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/shikoku/kochi/">高知</a>
                </div>
			    <div class="topMain_company_body_spListB">
                  <a href="<?php bloginfo('url'); ?>/area/kyushu/">九州・沖縄</a>
                </div>
				<div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kyushu/fukuoka/">福岡</a>
                </div>
				<div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kyushu/kumamoto/">熊本</a>
				</div>
				<div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kyushu/kagoshima/">鹿児島</a>
                </div>
				<div class="topMain_company_body_spList">
				  <a href="<?php bloginfo('url'); ?>/area/kyushu/nagasaki/">長崎</a>
                </div>
				<div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kyushu/oita/">大分</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kyushu/miyazaki/">宮崎</a>
                </div>
				<div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kyushu/saga/">佐賀</a>
                </div>
                <div class="topMain_company_body_spList">
                  <a href="<?php bloginfo('url'); ?>/area/kyushu/okinawa/">沖縄</a>
                </div>
              </div>
			</div>
          </section>
		  <section class="topSearchStatus">
            <div class="topMain_SectionTitle">
              <h2>雇用形態から探す</h2>
              <div class="topBtn_goIndex pc">
                <a href="/company">求人情報一覧へ</a>
              </div>
			</div>
			<div class="topMain_columnInr">
              <ul class="topMain_columnInr_lists">
				<li><a href="<?php bloginfo('url'); ?>/employee/regular/">正社員</a></li>
				<li><a href="<?php bloginfo('url'); ?>/employee/part/">パート・アルバイト</a></li>
				<li><a href="<?php bloginfo('url'); ?>/employee/contract/">契約社員</a></li>
				<li><a href="<?php bloginfo('url'); ?>/employee/free-lance/">業務委託</a></li>
				<li><a href="<?php bloginfo('url'); ?>/employee/shinsotsu/">新卒社員</a></li>
				<li><a href="<?php bloginfo('url'); ?>/employee/intern/">インターン</a></li>

				</ul>
            </div>
          </section>
		  <section class="topSearchCommitment">
            <div class="topMain_SectionTitle">
              <h2>こだわり条件から探す</h2>
              <div class="topBtn_goIndex pc">
                <a href="/company">求人情報一覧へ</a>
              </div>
			</div>
			<div class="topMain_columnInr">
              <ul class="topMain_columnInr_lists">
				  <li><a href="<?php bloginfo('url'); ?>/condition/beginner/">未経験OK</a></li>
				  <li><a href="<?php bloginfo('url'); ?>/condition/woman/">女性・ママ活躍中</a></li>
				  <li><a href="<?php bloginfo('url'); ?>/condition/short_time/">時短/フレックス制</a></li>
				  <li><a href="<?php bloginfo('url'); ?>/condition/accsess/">駅近・アクセス&#x25CE;</a></li>
				  <li><a href="<?php bloginfo('url'); ?>/condition/commission/">賞与有・歩合制</a></li>
				  <li><a href="<?php bloginfo('url'); ?>/condition/kanbu/">幹部候補・高所得</a></li>
				  <li><a href="<?php bloginfo('url'); ?>/condition/welfare/">福利厚生&#x25CE;</a></li>
				  <li><a href="<?php bloginfo('url'); ?>/condition/long_holiday/">長期休暇あり</a></li>
				  <li><a href="<?php bloginfo('url'); ?>/condition/foreigner/">外国人歓迎</a></li>
				  <li><a href="<?php bloginfo('url'); ?>/condition/qualification/">資格サポート</a></li>
				  <li><a href="<?php bloginfo('url'); ?>/condition/holiday/">海外勤務あり</a></li>
				  <li><a href="<?php bloginfo('url'); ?>/condition/overseas/">土日休み</a></li>
				</ul>
            </div>
          </section>
		  <section class="topSearchKeyword">
            <div class="topMain_SectionTitle">
              <h2>キーワードから探す</h2>
            </div>
			<div>
 			  <?php get_template_part('templete/searchbox'); ?>
			</div>
          </section>
        </div>
        <div class="side-col pc">
          <section class="side_ranking">
		    <?php get_sidebar(); ?>
          </section>
        </div>
      </div>
      
      <!-- /2カラムここまで -->
      
      <section class="topRecommend">
	    <div>
		  <div class="sectionTitle balloon2">
            <h2>厳選！レジャリストおすすめ求人</h2>
          </div>
		  <div class="topRecommend_inner">
			<?php $args = array(
            'post_type'=>'detail',
            'posts_per_page'=>12,
            'orderby'=>'date',
            'order'=>'DESC',
            'tax_query'=>array(
                array(
                    'taxonomy'=>'option',
                    'field'=>'slug',
                    'terms'=>'gensen',
                    ),
                ),
            );
            $posts = get_posts( $args );
            if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
			<div class="item"><!-- item -->
              <a href="<?php the_permalink(); ?>">
                <?php
                $image_id = get_post_thumbnail_id();
                $image_url = wp_get_attachment_image_src($image_id, true);
                ?>
                <figure><img src="<?php echo $image_url[0]; ?>" width="100%" alt=""/></figure>
                <div class="para">
                  <h3><?php $title= mb_substr($post->post_title,0,30); echo $title;?></h3>
                  <div class="particularTxt">
                  <?php $terms = get_the_terms( get_the_ID(), 'condition' ); ?>
                    <?php if(!empty($terms)): ?>
                      <?php for($i=0; $i<8; $i++): ?>
                        <?php if(isset($terms[$i])): ?>
                          <?php echo $terms[$i]->name; ?>｜
                          <?php else: break;?>
                        <?php endif; ?>
                      <?php endfor; ?>
                    <?php endif; ?>
                  </div>
                  <div class="pcommentTxt"><?php echo post_custom('maincatch'); ?></div>
                </div>
              </a>
            </div><!-- /item -->
        <?php endforeach; ?>
        <?php endif; wp_reset_postdata(); ?>
		  </div>
		  
		  <script>
            $(function() {
                $('.topRecommend_inner').slick({
                    infinite: true,
                    dots:false,
                    slidesToShow: 4,
                    slidesToScroll: 1,
					autoplay:  true,
					pauseOnHover: true,
					responsive: [
                    {
                        breakpoint: 1220, //1220px以下のサイズに適用
                        settings: {
                        slidesToShow:3,
						}
                    },
					{
                        breakpoint: 768, //767px以下のサイズに適用
                        settings: {
                        slidesToShow:2,
						centerMode: true,
						centerPadding: '20px'	
                        }
                    }
						
                    ]
                });
            });
          </script>
	    </div>
      </section>
      
      <section class="topFeature">
	    <div>
		  <div class="sectionTitle balloon3">
            <h2>インタビュー特設コーナー</h2>
          </div>
		  <div class="topFeature_inner">
			<?php $args = array(
            'post_type'=>'feature',
            'posts_per_page'=>6,
            'orderby'=>'date',
            'order'=>'DESC',
            );
            $posts = get_posts( $args );
            if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
			<!-- item -->
			<div class="item">
              <a href="<?php the_permalink(); ?>">
                <?php
                $image_id = get_post_thumbnail_id();
                $image_url = wp_get_attachment_image_src($image_id, true);
                ?>
                <div class="featureThumb">
				  <figure>
				    <img src="<?php echo $image_url[0]; ?>" width="100%" alt=""/>
                    <figucaption>
                    <?php
                        if ($terms = get_the_terms($post->ID, 'interview_gyosyu')) 
                        foreach ( $terms as $term ): // foreach ループの開始
                        ?>
                        <?php echo $term->name; ?>
                    <?php endforeach; ?>
                    </figucaption>
				  </figure>
				  <div class="featureThumb-txt">
                    <h3><?php $title= mb_substr($post->post_title,0,40); echo $title;?></h3>
                    <p><?php echo mb_substr(strip_tags($post-> post_content),0,30).'...'; ?></p>
                  </div>
				</div>
              </a>
            </div>
        <?php endforeach; ?>
        <?php endif; wp_reset_postdata(); ?>
		  </div><!-- /item -->
		  <div class="featureBtn">
			<a href="<?php bloginfo('url'); ?>/feature/">
			  <span>インタビュー一覧へ</span>
			</a>
		  </div>
	    </div>
      </section>
     
      <section class="topColumn">
	    <div>
		  <div class="sectionTitle balloon4">
            <h2>転職お役立ちコラム</h2>
          </div>
		  <div class="topColumn_inner">
			<?php $args = array(
            'post_type'=>'column',
            'posts_per_page'=>2,
            'orderby'=>'date',
            'order'=>'DESC',
            );
            $posts = get_posts( $args );
            if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
			<div class="item"><!-- item -->
              <a href="<?php the_permalink(); ?>">
                <?php
                $image_id = get_post_thumbnail_id();
                $image_url = wp_get_attachment_image_src($image_id, true);
                ?>
                <div class="columnThumb">
				  <figure>
				    <img src="<?php echo $image_url[0]; ?>" width="100%" alt=""/>
                  </figure>
				  <div  class="columnPara">
					<div class="columnTag">
					  <?php
                        if ($terms = get_the_terms($post->ID, 'column_tag')) 
                        foreach ( $terms as $term ): // foreach ループの開始
                        ?>
                        <?php echo $term->name; ?>
                      <?php endforeach; ?>
					</div>
					<hr class="topColumnHr">
					<div class="columnParaInr">
				      <h3><?php $title= mb_substr($post->post_title,0,40); echo $title;?></h3>
				      <p><?php echo mb_substr(strip_tags($post-> post_content),0,40) . '...'; ?></p>
					</div>
				  </div>
				</div>
              </a>
            </div><!-- /item -->
        <?php endforeach; ?>
        <?php endif; wp_reset_postdata(); ?>
		  </div>
		  <div class="columnBtn">
			<a href="<?php bloginfo('url'); ?>/column/">
			  <span>コラム一覧へ</span>
			</a>
		  </div>
	    </div>
      </section>

      <!-- 2カラム -->
      <div class="main-wid">
      <section class="topSearchCompany">
          <div class="topMain_SectionTitle">
            <h2 class="agent">旅行会社から探す</h2>
          </div>
          <div class="topSearchCompanyInr">
			 <?php $args = array(
                'post_type'=>'company',
                'posts_per_page'=>8,
                'orderby'=>'date',
                'order'=>'DESC',
                'tax_query'=>array(
                    array(
                        'taxonomy'=>'companygyosyu',
                        'field'=>'slug',
                        'terms'=>'company_agent',
                        ),
                    ),
                );
                $posts = get_posts( $args );
                if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
                <div class="top-company_flo">
                    <a href="<?php the_permalink(); ?>">
                    <?php if( get_field('logo') ): ?>
                        <div style="background-color:#fff;background:url(<?php the_field('logo'); ?>);background-size: 80% auto;background-repeat: no-repeat;width:100%;padding-top:60%;background-position: center center;" class="top-company_flo_logo"></div>
                    <?php endif; ?>
                    </a>
                    
                </div>
            <?php endforeach; ?>
            <?php endif; wp_reset_postdata(); ?>
		  </div>
          <div class="topMain_SectionTitle">
            <h2 class="online">オンライン系／OTA会社から探す</h2>
          </div>
		  <div class="topSearchCompanyInr">
            <?php $args = array(
                'post_type'=>'company',
                'posts_per_page'=>8,
                'orderby'=>'date',
                'order'=>'DESC',
                'tax_query'=>array(
                    array(
                        'taxonomy'=>'companygyosyu',
                        'field'=>'slug',
                        'terms'=>'company_online',
                        ),
                    ),
                );
                $posts = get_posts( $args );
                if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
                <div class="top-company_flo">
                    <a href="<?php the_permalink(); ?>">
                    <?php if( get_field('logo') ): ?>
                        <div style="background-color:#fff;background:url(<?php the_field('logo'); ?>);background-size: 80% auto;background-repeat: no-repeat;width:100%;padding-top:60%;background-position: center center;" class="top-company_flo_logo"></div>
                    <?php endif; ?>
                    </a>
			    </div>
            <?php endforeach; ?>
            <?php endif; wp_reset_postdata(); ?>
          </div>
          <div class="topMain_SectionTitle">
            <h2 class="hotel">ホテル会社から探す</h2>
          </div>
		  <div class="topSearchCompanyInr">
            <?php $args = array(
                'post_type'=>'company',
                'posts_per_page'=>8,
                'orderby'=>'date',
                'order'=>'DESC',
                'tax_query'=>array(
                    array(
                        'taxonomy'=>'companygyosyu',
                        'field'=>'slug',
                        'terms'=>'company_hotel',
                        ),
                    ),
                );
                $posts = get_posts( $args );
                if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
                <div class="top-company_flo">
                    <a href="<?php the_permalink(); ?>">
                    <?php if( get_field('logo') ): ?>
                        <div style="background-color:#fff;background:url(<?php the_field('logo'); ?>);background-size: 80% auto;background-repeat: no-repeat;width:100%;padding-top:60%;background-position: center center;" class="top-company_flo_logo"></div>
                    <?php endif; ?>
                    </a>
			    </div>
            <?php endforeach; ?>
            <?php endif; wp_reset_postdata(); ?>
		  </div>
          <div class="topMain_SectionTitle">
            <h2 class="ryokan">旅館会社から探す</h2>
          </div>
		  <div class="topSearchCompanyInr">
            <?php $args = array(
                'post_type'=>'company',
                'posts_per_page'=>8,
                'orderby'=>'date',
                'order'=>'DESC',
                'tax_query'=>array(
                    array(
                        'taxonomy'=>'companygyosyu',
                        'field'=>'slug',
                        'terms'=>'company_ryokan',
                        ),
                    ),
                );
                $posts = get_posts( $args );
                if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
                <div class="top-company_flo">
                    <a href="<?php the_permalink(); ?>">
                    <?php if( get_field('logo') ): ?>
                        <div style="background-color:#fff;background:url(<?php the_field('logo'); ?>);background-size: 80% auto;background-repeat: no-repeat;width:100%;padding-top:60%;background-position: center center;" class="top-company_flo_logo"></div>
                    <?php endif; ?>
                    </a>
			    </div>
		    <?php endforeach; ?>
            <?php endif; wp_reset_postdata(); ?>
		  </div>
        </section>
      </div>

    </main>
  </div>
  <div class="top-footup">
        <div class="top-footup-inr">
		  <p class="top-footup-inr-l">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/top/bnr_logo.png" alt="レジャリスト">
		  </p>
		  <p class="top-footup-inr-r">
			  レジャリストは、旅行会社、ホテル・旅館、観光ドライバーなど、観光業界（レジャー業界）に特化した<strong>業界初の求人転職サイト</strong>です！<br>
            旅行会社については、第1種旅行業、第2種旅行業、第3種旅行業だけでなく、オンライン専門のOTAに関する募集要項まで掲載しております。旅行の企画プランナーや営業、手配事務などのお仕事はもちろん、マーケティングやエンジニア/クリエイターなどのお仕事まで、幅広い求人案件をご紹介することができます！<br>
            ホテル旅館においても、ビジネスホテルはもちろんシティホテルやリゾートホテルなど、一流ホテルのホテルマンに関する求人から、日本を代表する老舗旅館の求人案件まで。さまざまな職種の求人案件をわかりやすく掲載しています。
		  </p>
		</div>
		<div class="top-footup-inr">
		　<p class="">
            なぜそれが実現できるのか。それは旅行業界、ホテル旅館業界、観光ドライバー業界など、様々な業種が存在する観光業において、各業種のお仕事を現場レベル、肌感覚で精通しているプロフェッショナルが、キャリアアドバイザーを担当しているから！だから求職者様のご希望通りの就職先を高確率でマッチング！観光旅行業界に興味のある求職者様や熱意のある求職者に対しては、ピッタリな求人案件をご提案する自身がありますので、ぜひキャリアアドバイザーにご相談くださいね！<br>
          </p>
		  <p>
            旅行業界やホテル業界は、2020年に向けて市場も急成長を続けておりますし、国も「観光立国」を掲げ、訪日観光客やインバウンド需要によって国を成長させる後押しをしている分野でもあります。既に観光業界で活躍している方のキャリアアップはもちろん、旅行業界⇆ホテル旅館業界での転職で、ご自身の経験を活かした即戦力採用もOKです！未経験だけど観光業界や旅行業界でこれからのキャリアをお考えの方も、先ずは気軽に電話・メールでレジャリストに相談してみてください♪
            旅行会社のお仕事、ホテル旅館のお仕事探しは【レジャリスト 】
		  </p>
        </div>
    </div>
 
</div>

<?php
get_footer(); ?>
