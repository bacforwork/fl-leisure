<?php
/**
 * travel_job functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package travel_job
 */

if ( ! function_exists( 'travel_job_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function travel_job_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on travel_job, use a find and replace
		 * to change 'travel_job' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'travel_job', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'travel_job' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'travel_job_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'travel_job_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function travel_job_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'travel_job_content_width', 640 );
}
add_action( 'after_setup_theme', 'travel_job_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function travel_job_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'travel_job' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'travel_job' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar-other', 'travel_job' ),
		'id'            => 'sidebar-2',
		'description'   => esc_html__( 'Add widgets here.', 'travel_job' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'travel_job_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function travel_job_scripts() {
	wp_enqueue_style( 'travel_job-style', get_stylesheet_uri() );

	wp_enqueue_script( 'travel_job-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'travel_job-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'travel_job-keeplist', get_template_directory_uri() . '/js/keeplist.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'travel_job_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function is_mobile() {
	$useragents = array(
	  'iPhone',          // iPhone
	  'iPod',            // iPod touch
	  'Android',         // 1.5+ Android
	  'dream',           // Pre 1.5 Android
	  'CUPCAKE',         // 1.5+ Android
	  'blackberry9500',  // Storm
	  'blackberry9530',  // Storm
	  'blackberry9520',  // Storm v2
	  'blackberry9550',  // Storm v2
	  'blackberry9800',  // Torch
	  'webOS',           // Palm Pre Experimental
	  'incognito',       // Other iPhone browser
	  'webmate'          // Other iPhone browser
	);
	$pattern = '/'.implode('|', $useragents).'/i';
	return preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
  }

  add_filter('page_template_hierarchy', 'my_page_templates');
  function my_page_templates($templates) {
	  global $wp_query;

	  $template = get_page_template_slug();
	  $pagename = $wp_query->query['pagename'];

	  if ($pagename && ! $template) {
		  $pagename = str_replace('/', '__', $pagename);
		  $decoded = urldecode($pagename);

		  if ($decoded == $pagename) {
			  array_unshift($templates, "page-{$pagename}.php");
		  }
	  }

	  return $templates;
  }

  function get_post_number( $post_type = 'customer', $op = '<=' ) {
    global $wpdb, $post;
    $post_type = is_array($post_type) ? implode("','", $post_type) : $post_type;
    $number = $wpdb->get_var("
        SELECT COUNT( * )
        FROM $wpdb->posts
        WHERE post_date {$op} '{$post->post_date}'
        AND post_status = 'publish'
        AND post_type = ('{$post_type}')
    ");
    return $number;
}

function disable_emoji() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	}
	add_action( 'init', 'disable_emoji' );


//////////////////////////////////////////////////
//オリジナル目次の表示非表示
//////////////////////////////////////////////////
// CheckBox
function fit_sanitize_checkbox( $checked ) {
	return ( ( isset( $checked ) && true == $checked ) ? true : false );
}
// radio/select
function fit_sanitize_select( $input, $setting ) {
 $input = sanitize_key( $input );
	$choices = $setting->manager->get_control($setting->id)->choices;
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}
// number limit
function fit_sanitize_number_range( $number, $setting ) {
	$number = absint( $number );
	$atts = $setting->manager->get_control( $setting->id )->input_attrs;
	$min = ( isset( $atts['min'] ) ? $atts['min'] : $number );
	$max = ( isset( $atts['max'] ) ? $atts['max'] : $number );
	$step = ( isset( $atts['step'] ) ? $atts['step'] : 1 );
	return ( $min <= $number && $number <= $max && is_int( $number / $step ) ? $number : $setting->default );
}

//////////////////////////////////////////////////
//オリジナル目次を作成
//////////////////////////////////////////////////
function get_outline_info($content) {
	// 目次のHTMLを入れる変数を定義します。
	$outline = '';
	// h1?h6タグの個数を入れる変数を定義します。
	$counter = 0;
		// 記事内のh1?h6タグを検索します。(idやclass属性も含むように改良)
		if (preg_match_all('/<h([1-2])[^>]*>(.*?)<\/h\1>/', $content, $matches,  PREG_SET_ORDER)) {
			// 記事内で使われているh1?h6タグの中の、1?6の中の一番小さな数字を取得します。
			// ※以降ソースの中にある、levelという単語は1?6のことを表します。
			$min_level = min(array_map(function($m) { return $m[1]; }, $matches));
			// スタート時のlevelを決定します。
			// ※このレベルが上がる毎に、<ul></li>タグが追加されていきます。
			$current_level = $min_level - 1;
			// 各レベルの出現数を格納する配列を定義します。
			$sub_levels = array('1' => 0, '2' => 0, '3' => 0, '4' => 0);
			// 記事内で見つかった、hタグの数だけループします。
			foreach ($matches as $m) {
				$level = $m[1];  // 見つかったhタグのlevelを取得します。
				$text = $m[2];  // 見つかったhタグの、タグの中身を取得します。
				// li, ulタグを閉じる処理です。2ループ目以降に中に入る可能性があります。
				// 例えば、前回処理したのがh3タグで、今回出現したのがh2タグの場合、
				// h3タグ用のulを閉じて、h2タグに備えます。
				while ($current_level > $level) {
					$current_level--;
					$outline .= '</li></ul>';
				}
				// 同じlevelの場合、liタグを閉じ、新しく開きます。
				if ($current_level == $level) {
					$outline .= '</li><li class="outline__item">';
				} else {
					// 同じlevelでない場合は、ul, liタグを追加していきます。
					// 例えば、前回処理したのがh2タグで、今回出現したのがh3タグの場合、
					// h3タグのためにulを追加します。
					while ($current_level < $level) {
						$current_level++;
						$outline .= sprintf('<ul class="outline__list outline__list-%s"><li class="outline__item">', $current_level);
					}
					// 見出しのレベルが変わった場合は、現在のレベル以下の出現回数をリセットします。
					for ($idx = $current_level + 0; $idx < count($sub_levels); $idx++) {
						$sub_levels[$idx] = 0;
					}
				}
				// 各レベルの出現数を格納する配列を更新します。
				$sub_levels[$current_level]++;
				// 現在処理中のhタグの、パスを入れる配列を定義します。
				// 例えば、h2 -> h3 -> h3タグと進んでいる場合は、
				// level_fullpathはarray(1, 2)のようになります。
				// ※level_fullpath[0]の1は、1番目のh2タグの直下に入っていることを表します。
				// ※level_fullpath[1]の2は、2番目のh3を表します。
				$level_fullpath = array();
				for ($idx = $min_level; $idx <= $level; $idx++) {
					$level_fullpath[] = $sub_levels[$idx];
				}
				$target_anchor = 'outline__' . implode('_', $level_fullpath);

				// 目次に、<a href="#outline_1_2">1.2 見出し</a>のような形式で見出しを追加します。
				$outline .= sprintf('<a class="outline__link" href="#%s"><span class="outline__number">0%s</span> %s</a>', $target_anchor, implode('.', $level_fullpath), strip_tags($text));
				// 本文中の見出し本体を、<h3>見出し</h3>を<h3 id="outline_1_2">見出し</h3>
				// のような形式で置き換えます。
				$hid = preg_replace('/<h([1-2])/', '<h\1 id="' .$target_anchor . '"', $m[0]);
				$content = str_replace($m[0], $hid, $content);

			}
			// hタグのループが終了後、閉じられていないulタグを閉じていきます。
			while ($current_level >= $min_level) {
				$outline .= '</li></ul>';
				$current_level--;
			}
			// h1?h6タグの個数
			$counter = count($matches);
		}
		return array('content' => $content, 'outline' => $outline, 'count' => $counter);
	}

	//目次を作成します。
	function add_outline($content) {

		// 目次を表示するために必要な見出しの数
	if(get_option('fit_post_outline_number')){
	$number = get_option('fit_post_outline_number');
	}else{
	$number = 1;
	}
		// 目次関連の情報を取得します。
		$outline_info = get_outline_info($content);
		$content = $outline_info['content'];
		$outline = $outline_info['outline'];
		$count = $outline_info['count'];
	if (get_option('fit_post_outline_close') ) {
	$close = "";
	}else{
	$close = "checked";
	}
		if ($outline != '' && $count >= $number) {
			// 目次を装飾します。
			$decorated_outline = sprintf('
	<div class="outline">
		<div align="center">
			<span class="outline__title">目次</span>
			<input class="outline__toggle" id="outline__toggle" type="checkbox" '.$close.'>
			<label class="outline__switch" for="outline__toggle"></label>
			%s
		</div>
	</div>', $outline);
			// カスタマイザーで目次を非表示にする以外が選択された時＆個別非表示が1以外の時に目次を追加します。
	if ( get_option('fit_post_outline') != 'value2' && get_post_meta(get_the_ID(), 'outline_none', true) != '1' && is_single() ) {
			$shortcode_outline = '[outline]';
			if (strpos($content, $shortcode_outline) !== false) {
				// 記事内にショートコードがある場合、ショートコードを目次で置換します。
				$content = str_replace($shortcode_outline, $decorated_outline, $content);
			} else if (preg_match('/<h[1-6].*>/', $content, $matches, PREG_OFFSET_CAPTURE)) {
				// 最初のhタグの前に目次を追加します。
				$pos = $matches[0][1];
				$content = substr($content, 0, $pos) . $decorated_outline . substr($content, $pos);
			}
	}
		}
	return $content;
	}
	add_filter('the_content', 'add_outline');


	function override_mce_options( $init_array ) {
		global $allowedposttags;
		$init_array['valid_elements']          = '*[*]';
		$init_array['extended_valid_elements'] = '*[*]';
		$init_array['valid_children']          = '+a[' . implode( '|', array_keys( $allowedposttags ) ) . ']';
		$init_array['indent']                  = true;
		$init_array['wpautop']                 = false;
		return $init_array;
	}
	add_filter( 'tiny_mce_before_init', 'override_mce_options' );




/**
 * 検索にカスタムフィールドを含める　会社情報
 */
function custom_search($search, $wp_query) {
	global $wpdb;
	if (!$wp_query->is_search)
			return $search;
	if (!isset($wp_query->query_vars))
			return $search;
	$search_words = explode(' ', isset($wp_query->query_vars['s']) ? $wp_query->query_vars['s'] : '');
	if ( count($search_words) > 0 ) {
			$search = '';
			$search .= " AND (post_type = 'company' OR post_type='detail')";
			foreach ( $search_words as $word ) {
					if ( !empty($word) ) {
							$search_word = '%' . esc_sql( $word ) . '%';
							$search .= " AND (
								 {$wpdb->posts}.post_title LIKE '{$search_word}'
								OR {$wpdb->posts}.post_content LIKE '{$search_word}'
								OR {$wpdb->posts}.ID IN (
								SELECT distinct post_id
								FROM {$wpdb->postmeta}
								WHERE meta_value LIKE '{$search_word}'
								)
							) ";
					}
			}
	}
	return $search;
}
add_filter('posts_search','custom_search', 10, 2);






//カスタム投稿タイプの公開時に自動で投稿IDをスラッグにセットする
function add_slug_for_posts($post_id) {
	global $wpdb;

	$posts_data = get_post($post_id, ARRAY_A);
	$slug = $posts_data['post_name'];
	if ($post_id != $slug){
		$my_post = array();
		$my_post['ID'] = $post_id;
		$my_post['post_name'] = $post_id;
	wp_update_post($my_post);
	}
	}
	add_action('publish_detail', 'add_slug_for_posts');

/* contact form7 値引き渡し*/
function my_form_tag_filter($tag){
  if ( ! is_array( $tag ) )
  return $tag;
  if(isset($_POST['tenpo'])){
    $name = $tag['name'];
    if($name == 'tenpo')
      $tag['values'] = (array) $_POST['tenpo'];
  }
  if(isset($_POST['company-tit'])){
    $name = $tag['name'];
    if($name == 'company-tit')
      $tag['values'] = (array) $_POST['company-tit'];
	}
	if(isset($_POST['url'])){
    $name = $tag['name'];
    if($name == 'url')
      $tag['values'] = (array) $_POST['url'];
  }
	if(isset($_POST['jobtype'])){
    $name = $tag['name'];
    if($name == 'jobtype')
      $tag['values'] = (array) $_POST['jobtype'];
  }
	if(isset($_POST['employee'])){
    $name = $tag['name'];
    if($name == 'employee')
      $tag['values'] = (array) $_POST['employee'];
  }
  return $tag;
}
add_filter('wpcf7_form_tag', 'my_form_tag_filter');


function add_cat_slug_class( $output, $args ) {
	$regex = '/<li class="cat-item cat-item-([\d]+)[^"]*">/';
	$taxonomy = isset( $args['taxonomy'] ) && taxonomy_exists( $args['taxonomy'] ) ? $args['taxonomy'] : 'station';
	preg_match_all( $regex, $output, $m );
	if ( ! empty( $m[1] ) ) {
			$replace = array();
			foreach ( $m[1] as $term_id ) {
					$term = get_term( $term_id, $taxonomy );
					if ( $term && ! is_wp_error( $term ) ) {
							$replace['/<li class="cat-item cat-item-' . $term_id . '("| )/'] = '<li id="' . esc_attr( $term->slug ) . '$1';
					}
			}
			$output = preg_replace( array_keys( $replace ), $replace, $output );
	}
	return $output;
}
add_filter( 'wp_list_categories', 'add_cat_slug_class', 10, 2 );


function new_excerpt_more($more) {
	return '…';
}
add_filter('excerpt_more', 'new_excerpt_more');

//---------------------------------------//
// 内部リンクのブログカード化（ショートコード）
// ここから
//---------------------------------------//

// 記事IDを指定して抜粋文を取得する
function ltl_get_the_excerpt($post_id){
  global $post;
  $post_bu = $post;
  $post = get_post($post_id);
  setup_postdata($post_id);
  $output = get_the_excerpt();
  $post = $post_bu;
  return $output;
}

//内部リンクをはてなカード風にするショートコード
function nlink_scode($atts) {
	extract(shortcode_atts(array(
		'url'=>"",
		'title'=>"",
		'excerpt'=>""
	),$atts));

	$id = url_to_postid($url);//URLから投稿IDを取得

	$img_width ="120";//画像サイズの幅指定
	$img_height = "120";//画像サイズの高さ指定
	$no_image = 'https://leisure-ist.com/wp-content/uploads/08.png';//アイキャッチ画像がない場合の画像を指定

	//タイトルを取得
	if(empty($title)){
		$title = esc_html(get_the_title($id));
	}
    //抜粋文を取得
	if(empty($excerpt)){
		$excerpt = esc_html(ltl_get_the_excerpt($id));
	}

    //アイキャッチ画像を取得
    if(has_post_thumbnail($id)) {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id),array($img_width,$img_height));
        $img_tag = "<img src='" . $img[0] . "' alt='{$title}' width=" . $img[1] . " height=" . $img[2] . " />";
    }else{
    $img_tag ='<img src="'.$no_image.'" alt="" width="'.$img_width.'" height="'.$img_height.'" />';
    }

	$nlink .='
<div class="blog-card">
  <a href="'. $url .'">
      <div class="blog-card-thumbnail">'. $img_tag .'</div>
      <div class="blog-card-content">
          <div class="blog-card-title">'. $title .' </div>
          <div class="blog-card-excerpt">'. $excerpt .'</div>
      </div>
      <div class="clear"></div>
  </a>
</div>';

	return $nlink;
}
add_shortcode("nlink", "nlink_scode");


//---------------------------------------//
// 人気記事ランキング
//---------------------------------------//
function my_custom_popular_posts($post_id) {
	$count_key = 'popular_posts';
	$count = get_post_meta($post_id, $count_key, true);
	if ($count == '') {
					$count = 0;
					delete_post_meta($post_id, $count_key);
					add_post_meta($post_id, $count_key, '0');
	} else {
					$count++;
					update_post_meta($post_id, $count_key, $count);
	}
}
function my_custom_track_posts($post_id) {
	if (!is_single()) return;
	if (empty($post_id)) {
					global $post;
					$post_id = $post->ID;
	}
	my_custom_popular_posts($post_id);
}
add_action('wp_head', 'my_custom_track_posts');

function get_keeplist() {
    if (!wp_verify_nonce($_REQUEST['nonce'], 'keeplist')) {
        wp_send_json([]);
        exit;
    }

    $results = [
        'keeplist' => [],
        'posts' => [],
    ];

    $keeplist = $_POST['keeplist'];

    if (empty($keeplist)) {
        wp_send_json($results);
        exit;
    }
    $args = array(
        'post_type' => array('detail'),
        'post_status' => array('publish'),
        'include' => $keeplist,
        'posts_per_page' => -1,
    );

    $posts = get_posts($args);
    $keeplistUpdate = [];
    global $post;
    foreach ($posts as $p) {
        $post = $p;
        ob_start();
        require(dirname(__FILE__) . '/templete/joblist-ajax.php');
        $results['posts'][] = ob_get_clean();
        $keeplistUpdate[] = $post->ID;
    }
    $results['keeplist'] = array_unique($keeplistUpdate);
    wp_send_json($results);
    exit;
}
add_action('wp_ajax_get_keeplist', 'get_keeplist');
add_action('wp_ajax_nopriv_get_keeplist', 'get_keeplist');

//カスタム投稿用post_typeセット
add_filter('template_include','custom_search_template');
	function custom_search_template($template){
		if ( is_search() ){	
			$post_types = get_query_var('post_type');
			foreach ( (array) $post_types as $post_type )
			$templates[] = "search-{$post_type}.php";
			$templates[] = 'search.php';
			$template = get_query_template('search',$templates);
		}
	return $template;
}

//Update Category count callback to include jobtype
function change_category_arg() {
	global $wpdb;
	if ( ! taxonomy_exists('area') )
		return false;
	$gt= get_terms('area', ['pad_counts' => true, 'hide_empty' => false]);
	foreach ($gt as $t) {
		$area = get_posts(array(
			'post_type'=>'detail',
			'posts_per_page'=>'-1',
			'tax_query'=>array(
				array('taxonomy'=>'area',
				'field'=>'id',
				'terms'=>$t->term_id
				)
			)
		));
		$count_at=count($area);
		$ud = "update $wpdb->term_taxonomy set count=$count_at where term_id=$t->term_id";
		$wpdb->query($ud);
	}
}
add_action( 'admin_init', 'change_category_arg' );