<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package travel_job
 */

get_header();
?>
 
 <div class="main-wid">
	<?php
		global $wp_query;
		$total_results = $wp_query->found_posts;
		$search_query = get_search_query();
	?>
	<div class="pan">
		<a href="<?php bloginfo('url'); ?>">TOP</a> > 検索結果
	</div>
	<div class="main-col sp-pad">

		<div class="top-tit marb1">
            <h1 class="tit02 tit-def">検索結果</h1>
		</div>
		
		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="ar-workbox marb4">
					<a href="<?php the_permalink(); ?>">
					<div class="ar-work-name">
						<p><?php echo post_custom('company'); ?></p>
						<?php the_title(); ?>
					</div>
					</a>
					<div class="ar-workbox-pad">
						<div class="ar-work-catch">
							<?php echo post_custom('catch'); ?>
						</div>
						<div class="ar-work-flo">
							<?php if( get_field('main-img') ): ?>
								<a href="<?php the_permalink(); ?>"><img src="<?php the_field('main-img'); ?>" width="100%" class="marb2"></a>
							<?php endif; ?>
						</div>
						<div class="ar-work-flo2">
							<div class="ar-work-table marb1">
								<table>
									<tr>
										<th>本社</th>
										<td><?php echo post_custom('address'); ?></td>
									</tr>
									<tr>
										<th>代表</th>
										<td><?php echo post_custom('daihyoname'); ?></td>
									</tr>
									<tr>
										<th>業種・業態</th>
										<td>
											<?php echo post_custom('gyosyu'); ?>
										</td>
									</tr>
								</table>
							</div>
							<a href="<?php the_permalink(); ?>">
							<div class="ar-work-btn">
								<p>詳しく見る</p>
							</div>
							</a>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			<?php endwhile; ?>
					
			<div class="pager">
				<?php global $wp_rewrite; $paginate_base = get_pagenum_link(1); if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
					$paginate_format = '';
					$paginate_base = add_query_arg('paged','%#%');
				}
				else{
					$paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
					user_trailingslashit('page/%#%/','paged');;
					$paginate_base .= '%_%';
				}
				echo paginate_links(array(
					'base' => $paginate_base,
					'format' => $paginate_format,
					'total' => $wp_query->max_num_pages,
					'mid_size' => 5,
					'current' => ($paged ? $paged : 1),
					'prev_text' => '«',
					'next_text' => '»',
				)); ?>
			</div>
		<?php else: ?>
			現在、「<?php echo $search_query; ?>」の情報はありません。
		<?php endif; ?>
	</div>
	<div class="side-col">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>
<?php
get_footer();