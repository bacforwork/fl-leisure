<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package travel_job
 */

get_header();
?>


<div class="main-wid">
	<div class="pan">
		<a href="<?php bloginfo('url'); ?>">TOP</a> > <a href="<?php bloginfo('url'); ?>/company">会社一覧</a> > <?php the_title(); ?>
	</div>
	<div class="main-col sp-pad marb4">
		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ) : the_post(); ?>



<!--------------------- 修正ここから --------------------->
              <h1 class="comName"><?php the_title(); ?></h1>
              <div class="comLogoBlock">
                <div class="comLogo"><img src="<?php the_field('logo'); ?>" width="100%" alt=""/></div>
                <div class="txt"><?php echo post_custom('catch'); ?></div>
              </div>

              <div class="main-slide">
				 <?php if( get_field('main-img') ): ?>
                   <div><img src="<?php the_field('main-img'); ?>" width="100%" alt=""/></div>
                 <?php endif; ?>
                 <?php if( get_field('main-img2') ): ?>
                   <div><img src="<?php the_field('main-img2'); ?>" width="100%"></div>
                 <?php endif; ?>
                 <?php if( get_field('main-img3') ): ?>
                   <div><img src="<?php the_field('main-img3'); ?>" width="100%"></div>
                 <?php endif; ?>
                 <?php if( get_field('main-img4') ): ?>
                   <div><img src="<?php the_field('main-img4'); ?>" width="100%"></div>
                 <?php endif; ?>
		      </div>
              <script>
                  $('.main-slide').slick({
                      autoplay:true,
                      autoplaySpeed:5000,
                      dots:true,
                  });
              </script>

<!-- メッセージ -->
              <h2 class="comTit comMessage"><?php the_title(); ?>からのメッセージ</h2>
              <div class="comMessageTxt">
              <?php echo post_custom('message'); ?>
              </div>

<!-- 会社の特徴・職場の雰囲気 -->
              <h2 class="comTit comAtmos">会社の特徴・職場の雰囲気</h2>
              <div class="atmosGraph">
                <dl>
                  <dt>平均年齢</dt>
                  <dd>
                    <?php if(get_post_meta($post->ID,'number',true) == '1'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarA01.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number',true) == '2'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarA02.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number',true) == '3'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarA03.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number',true) == '4'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarA04.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number',true) == '5'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarA05.png" width="100%">
                    <?php endif; ?>
                  </dd>
                </dl>
                <dl>
                  <dt>男女比率</dt>
                  <dd>
					<?php if(get_post_meta($post->ID,'number2',true) == '1'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarB01.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number2',true) == '2'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarB02.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number2',true) == '3'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarB03.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number2',true) == '4'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarB04.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number2',true) == '5'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarB05.png" width="100%">
                    <?php endif; ?>
				  </dd>
                </dl>
                <dl>
                  <dt>職場の雰囲気</dt>
                  <dd>
					<?php if(get_post_meta($post->ID,'number3',true) == '1'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarC01.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number3',true) == '2'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarC02.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number3',true) == '3'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarC03.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number3',true) == '4'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarC04.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number3',true) == '5'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarC05.png" width="100%">
                    <?php endif; ?>
				  </dd>
                </dl>
                <dl>
                  <dt>お客様の特徴</dt>
                  <dd>
					<?php if(get_post_meta($post->ID,'number4',true) == '1'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarD01.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number4',true) == '2'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarD02.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number4',true) == '3'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarD03.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number4',true) == '4'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarD04.png" width="100%">
                    <?php elseif (get_post_meta($post->ID,'number4',true) == '5'): ?>
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/company/rankbarD05.png" width="100%">
                    <?php endif; ?>
				  </dd>
                </dl>
              </div>

<!-- 社員からのメッセージ -->
              <h2 class="comTit memMessage"><?php the_title(); ?>社員からのメッセージ</h2>
              <div class="memMessageList">
                <ul>
                  <?php if( get_field('syainmessage') ): ?>
                    <li>
					  <a href="<?php the_field('syain_url'); ?>" target="_blank">
                        <figure><img src="<?php the_field('syainmessage'); ?>" width="100%" alt=""/></figure>
                        <div class="profTxt">
                          <h3><?php echo post_custom('syain_name'); ?>｜<?php echo post_custom('syain_syokusyu'); ?></h3>
                          <h4><?php echo nl2br(get_post_meta($post->ID, 'syain_text', true)); ?></h4>
                          <hr>
                          <p class="readMore">記事を読む</p>
                        </div>
                      </a>
					</li>
				  <?php endif; ?>
                  <?php if( get_field('syainmessage2') ): ?>
                    <li>
                      <a href="<?php the_field('syain_url2'); ?>" target="_blank">
                        <figure><img src="<?php the_field('syainmessage2'); ?>" width="100%" alt=""/></figure>
                        <div class="profTxt">
                          <h3><?php echo post_custom('syain_name2'); ?>｜<?php echo post_custom('syain_syokusyu2'); ?></h3>
                          <h4><?php echo nl2br(get_post_meta($post->ID, 'syain_text2', true)); ?></h4>
                          <hr>
                          <p class="readMore">記事を読む</p>
                        </div>
                      </a>
					</li>
				  <?php endif; ?>
				  <?php if( get_field('syainmessage3') ): ?>
                    <li>
                      <a href="<?php the_field('syain_url3'); ?>" target="_blank">
                        <figure><img src="<?php the_field('syainmessage3'); ?>" width="100%" alt=""/></figure>
                        <div class="profTxt">
                          <h3><?php echo post_custom('syain_name3'); ?>｜<?php echo post_custom('syain_syokusyu3'); ?></h3>
                          <h4><?php echo nl2br(get_post_meta($post->ID, 'syain_text3', true)); ?></h4>
                          <hr>
                          <p class="readMore">記事を読む</p>
                        </div>
                      </a>
					</li>
				  <?php endif; ?>
                </ul>
              </div>

<!-- よく分かるポイント -->
              <h2 class="comTit comPoint"><?php the_title(); ?>がよく分かるポイント</h2>
              <div class="comPointBlock">
                <?php if( get_field('pointtit1') ): ?>
		          <h3><?php echo post_custom('pointtit1'); ?></h3>
                  <div class="block">
                    <?php if( get_field('pointimg1') ): ?>
					  <figure><img src="<?php the_field('pointimg1'); ?>" width="100%" alt=""/></figure>
					<?php endif; ?>
                    <div class="txt">
                      <p><?php echo nl2br(get_post_meta($post->ID, 'pointtext1', true)); ?></p>
                    </div>
                  </div>
				<?php endif; ?>
				<?php if( get_field('pointtit2') ): ?>
		          <h3><?php echo post_custom('pointtit2'); ?></h3>
                  <div class="block">
                    <?php if( get_field('pointimg2') ): ?>
					  <figure><img src="<?php the_field('pointimg2'); ?>" width="100%" alt=""/></figure>
					<?php endif; ?>
                    <div class="txt">
                      <p><?php echo nl2br(get_post_meta($post->ID, 'pointtext2', true)); ?></p>
                    </div>
                  </div>
				<?php endif; ?>
				<?php if( get_field('pointtit3') ): ?>
		          <h3><?php echo post_custom('pointtit3'); ?></h3>
                  <div class="block">
                    <?php if( get_field('pointimg3') ): ?>
					  <figure><img src="<?php the_field('pointimg3'); ?>" width="100%" alt=""/></figure>
					<?php endif; ?>
                    <div class="txt">
                      <p><?php echo nl2br(get_post_meta($post->ID, 'pointtext3', true)); ?></p>
                    </div>
                  </div>
				<?php endif; ?>
				<?php if( get_field('pointtit4') ): ?>
		          <h3><?php echo post_custom('pointtit4'); ?></h3>
                  <div class="block">
                    <?php if( get_field('pointimg4') ): ?>
					  <figure><img src="<?php the_field('pointimg4'); ?>" width="100%" alt=""/></figure>
					<?php endif; ?>
                    <div class="txt">
                      <p><?php echo nl2br(get_post_meta($post->ID, 'pointtext4', true)); ?></p>
                    </div>
                  </div>
				<?php endif; ?>
				<?php if( get_field('pointtit5') ): ?>
		          <h3><?php echo post_custom('pointtit5'); ?></h3>
                  <div class="block">
                    <?php if( get_field('pointimg5') ): ?>
					  <figure><img src="<?php the_field('pointimg5'); ?>" width="100%" alt=""/></figure>
					<?php endif; ?>
                    <div class="txt">
                      <p><?php echo nl2br(get_post_meta($post->ID, 'pointtext5', true)); ?></p>
                    </div>
                  </div>
				<?php endif; ?>
		      </div>

<!-- 求人一覧 -->
		      <h2 class="recListTitle"><?php the_title(); ?>の求人一覧</h2>
              <div class="recList">
                <ul>
                  <?php
                  $term = array_shift(get_the_terms($post->ID, 'companyname'));
                  $args = array(
                      'post_type'=>'detail',
                      'posts_per_page'=>-1,
                      'orderby'=>'rand',
                      'tax_query'=>array(
                          array(
                              'taxonomy'=>'companyname',
                              'field'=>'slug',
                              'terms' => $term->slug,
                              ),
                          ),
                      );
                  $posts = get_posts( $args );
                  if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
                  <li>
                    <h3><?php the_title(); ?>｜<?php echo post_custom('company'); ?></h3>
                    <div class="recDetail">
                      <?php if( get_field('main-img') ): ?>
                        <figure><img src="<?php the_field('main-img'); ?>" width="282" height="212" alt=""/></figure>
                      <?php endif; ?>
                      <div class="txt">
                        <dl>
                          <dt class="place"><span>勤務地</span></dt>
                          <dd><?php echo post_custom('address1'); ?><?php echo post_custom('address2'); ?><?php echo post_custom('address3'); ?></dd>
                        </dl>
                        <dl>
                          <dt class="access"><span>アクセス</span></dt>
                          <dd><?php echo nl2br(get_post_meta($post->ID, 'access', true)); ?></dd>
                        </dl>
                        <dl>
                          <dt class="type"><span>雇用形態</span></dt>
                          <dd><?php echo post_custom('employee'); ?></dd>
                        </dl>
                        <dl>
                          <dt class="salary"><span>給与</span></dt>
                          <dd><?php echo post_custom('salarytype'); ?><?php echo post_custom('salary01'); ?>円～<?php if( get_field('salary02') ): ?><?php echo post_custom('salary02'); ?>円<?php endif; ?></dd>
                        </dl>
                        <dl>
                          <dt class="role"><span>募集職種</span></dt>
                          <dd><?php echo post_custom('jobtype'); ?></dd>
                        </dl>
                        <dl>
                          <dt class="particular"><span>こだわり</span></dt>
                          <dd>
                          <?php $terms = get_the_terms( get_the_ID(), 'condition' ); ?>
                            <?php if(!empty($terms)): ?>
                              <?php for($i=0; $i<8; $i++): ?>
                                <?php if(isset($terms[$i])): ?>
                                  <?php echo $terms[$i]->name; ?>｜
                                  <?php else: break;?>
                                <?php endif; ?>
                              <?php endfor; ?>
                            <?php endif; ?>
                          </dd>
                        </dl>
                      </div>
                    </div>
					<div class="comment">
                      <div class="tx">
                        <div class="appealTxt">
                          <?php echo post_custom('maincatch'); ?>
                        </div>
                      </div>
                      <div class="btns">
                        <a class="keeplist" href="javascript:void(0);" data-jobid="<?php echo get_the_ID(); ?>">
                          <div class="keepBtn">キープする</div>
                        </a>
                        <a href="<?php the_permalink(); ?>">
                          <div class="detailBtn">詳しく見る</div>
                        </a>
                      </div>
                    </div>
                  </li>
                    <?php endforeach; ?>
                  <?php endif; wp_reset_postdata(); ?>
                </ul>
              </div>

<!-- 法人情報 -->
              <h2 class="comTit comInfo">法人情報</h2>
              <div class="comInfoTable">
                <table>
                  <tr>
                    <th>会社名</th>
                    <td><?php echo post_custom('companyname'); ?></td>
                  </tr>
                  <tr>
                    <th>業種・業態</th>
                    <td><?php echo post_custom('gyosyu'); ?></td>
                  </tr>
                  <tr>
                    <th>本社</th>
                    <td><?php echo post_custom('address'); ?></td>
                  </tr>
                  <tr>
                    <th>創業</th>
                    <td><?php echo post_custom('setsuritsu'); ?></td>
                  </tr>
                  <tr>
                    <th>代表</th>
                    <td><?php echo post_custom('daihyoname'); ?></td>
                  </tr>
                  <tr>
                    <th>資本金</th>
                     <td><?php echo post_custom('shihonkin'); ?></td>
                  </tr>
                  <tr>
                    <th>従業員数</th>
                    <td><?php echo post_custom('jugyoin'); ?></td>
                  </tr>
                  <tr>
                    <th>支社・支店</th>
                    <td><?php echo post_custom('shiten'); ?></td>
                  </tr>
                  <tr>
                    <th>会社HP</th>
                    <td><a href="<?php echo post_custom('url'); ?>" target="_blank"><?php echo post_custom('url'); ?></a></td>
                  </tr>
                  <tr>
                    <th>公式SNS</th>
                    <td>
                    <?php echo nl2br(get_post_meta($post->ID, 'sns', true)); ?>
                    </td>
                  </tr>
                </table>
              </div>

<!-- Google Map -->
              <div class="gmapResponsive">
              <?php echo post_custom('map'); ?>
              </div>
            </div>
	
<!-- 島津追記 -->
              <?php endwhile; ?>
		    <?php endif; ?>	
	
            <div class="side-col"><!-- side-col -->
              <?php get_sidebar(); ?>
            </div><!-- /side-col -->
          </div>
<!--------------------- 修正ここまで --------------------->


<?php

get_footer();