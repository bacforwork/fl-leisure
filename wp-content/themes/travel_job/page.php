<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package travel_job
 */

get_header();
?>
<div class="main-wid">
	<div class="pan marb1">
		<a href="<?php bloginfo('url'); ?>">TOP</a> > <?php the_title(); ?>
	</div>
	<div class="main-col sp-pad marb4 page-sec">
		<div class="detail-tit">
			<h1><?php the_title(); ?></h1>
		</div>
		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
							
	<div class="side-col">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>


<?php
get_footer();
