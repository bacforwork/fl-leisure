<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package moumou
 */

if ( ! is_active_sidebar( 'sidebar-2' ) ) {
	return;
}
?>

  <div class="heading">
    <h2>現在の検索条件</h2>
    <div class="">
	  <?php $term_slug = get_query_var('jobtype');
		$term_info = get_term_by('slug',$term_slug,'jobtype');?><?php if ($term_info->parent) { $parent = get_term( $term_info->parent,'jobtype' );echo $parent->name;} ?>
		：<?php single_term_title(); ?>
	</div>
  </div>
  <h3>検索条件一覧</h3>
  <div class="acdBlock"><!-- acdBlock -->
    <input id="acd-check1" class="acd-check" type="checkbox" checked>
    <label class="acd-label first-child acd01" for="acd-check1">業種・職種から探す</label>
    <div class="acd-content">
      <h4 class="sub01">旅行会社</h4>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/agent/sales/">- 営業・セールス</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/agent/conductor/">- 添乗員・コンダクター</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/agent/guide/">- ガイド・通訳</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/agent/web/">- web担当・マーケター</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/agent/officework/">- 手配事務・経理</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/agent/driver/">- バスドライバー</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/agent/writer/">- ライター・クリエイター</a></li>
      </ul>
      <h4 class="sub02">オンライン / OTA</h4>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/online/sales-online/">- 営業・セールス</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/online/engineer/">- エンジニア</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/online/marketing/">- マーケティング</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/online/office-work/">- 手配事務・経理</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/online/planner-online/">- 企画・プランナー</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/online/designer/">- デザイナー</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/online/director/">- ディレクター</a></li>
      </ul>
      <h4 class="sub03">ホテル</h4>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/hotel/front/">- フロント</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/hotel/manager/">- マネージャー</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/hotel/reception/">- 接客係</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/hotel/sales-hotel/">- 営業・セールス</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/hotel/officework-hotel/">- 予約受付・事務</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/hotel/restaurant/">- レストラン</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/hotel/cock/">- 調理スタッフ</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/hotel/kitchen/">- 洗い場スタッフ</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/hotel/cleaning/">- 清掃スタッフ</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/hotel/other/">- その他</a></li>
      </ul>
      <h4 class="sub04">旅館</h4>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/ryokan/front-ryokan/">- フロント</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/ryokan//manager-ryokan/">- 支配人（幹部候補）</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/ryokan/nakai/">- 仲居（客室係）</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/ryokan/sales-ryokan/">- 営業・セールス</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/ryokan/officework-ryokan/">- 予約受付・事務</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/ryokan/restaurant-ryokan/">- レストラン</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/ryokan/cock-ryokan/">- 調理スタッフ</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/ryokan/kitchen-ryokan/">- 洗い場スタッフ</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/ryokan/cleaning-ryokan/">- 清掃スタッフ</a></li>
        <li><a href="<?php bloginfo('url'); ?>/jobtype/ryokan/other-ryokan/">- その他</a></li>
      </ul>
    </div>

    <input id="acd-check2" class="acd-check" type="checkbox">
    <label class="acd-label acd02" for="acd-check2">エリアから探す</label>
    <div class="acd-content">
      <h4 class="sub0">関東</h4>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/area/kanto/tokyo/">- 東京</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kanto/kanagawa/">- 神奈川</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kanto/saitama/">- 埼玉</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kanto/chiba/">- 千葉</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kanto/ibaraki/">- 茨城</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kanto/tochigi/">- 栃木</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kanto/gunma/">- 群馬</a></li>
      </ul>
	  <h4 class="sub0">関西</h4>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/area/kansai/osaka/">- 大阪</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kansai/hyogo/">- 兵庫</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kansai/kyoto/">- 京都</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kansai/shiga/">- 滋賀</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kansai/nara/">- 奈良</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kansai/wakayama/">- 和歌山</a></li>
      </ul>
	  <h4 class="sub0">東海</h4>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/area/tokai/aichi/">- 愛知</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/tokai/shizuoka/">- 静岡</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/tokai/gifu/">- 岐阜</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/tokai/mie/">- 三重</a></li>
      </ul>
	  <h4 class="sub0">北海道・東北</h4>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/area/tohoku/hokkaido/">- 北海道</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/tohoku/miyagi/">- 宮城</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/tohoku/fukushima/">- 福島</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/tohoku/aomori/">- 青森</a></li>
		<li><a href="<?php bloginfo('url'); ?>/area/tohoku/iwate/">- 岩手</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/tohoku/yamagata/">- 山形</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/tohoku/akita/">- 秋田</a></li>
      </ul>
	  <h4 class="sub0">北陸・甲信越</h4>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/area/hokuriku/niigata/">- 新潟</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/hokuriku/nagano/">- 長野</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/hokuriku/ishikawa/">- 石川</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/hokuriku/toyama/">- 富山</a></li>
		<li><a href="<?php bloginfo('url'); ?>/area/hokuriku/yamanashi/">- 山梨</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/hokuriku/fukui/">- 福井</a></li>
      </ul>
	  <h4 class="sub0">中国</h4>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/area/chugoku/hiroshima/">- 広島</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/chugoku/okayama/">- 岡山</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/chugoku/yamaguchi/">- 山口</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/chugoku/shimane/">- 島根</a></li>
		<li><a href="<?php bloginfo('url'); ?>/area/chugoku/tottori/">- 鳥取</a></li>
      </ul>
	  <h4 class="sub0">四国</h4>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/area/shikoku/ehime/">- 愛媛</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/shikoku/kagawa/">- 香川</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/shikoku/tokushima/">- 徳島</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/shikoku/kochi/">- 高知</a></li>
	  </ul>
	  <h4 class="sub0">九州・沖縄</h4>
      <ul>
        <li><a href="<?php bloginfo('url'); ?>/area/kyushu/fukuoka/">- 福岡</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kyushu/kumamoto/">- 熊本</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kyushu/kagoshima/">- 鹿児島</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kyushu/nagasaki/">- 長崎</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kyushu/oita/">- 大分</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kyushu/miyazaki/">- 宮崎</a></li>
        <li><a href="<?php bloginfo('url'); ?>/area/kyushu/saga/">- 佐賀</a></li>
		<li><a href="<?php bloginfo('url'); ?>/area/kyushu/okinawa/">- 沖縄</a></li>
      </ul>
    </div>

    <input id="acd-check3" class="acd-check" type="checkbox">
    <label class="acd-label acd03" for="acd-check3">雇用形態から探す</label>
    <div class="acd-content">
	  <a href="<?php bloginfo('url'); ?>/employee/regular/"><h4 class="sub05">正社員</h4></a>
      <a href="<?php bloginfo('url'); ?>/employee/part/"><h4 class="sub05">パート・アルバイト</h4></a>
	  <a href="<?php bloginfo('url'); ?>/employee/contract/"><h4 class="sub05">契約社員</h4></a>
    </div>

    <input id="acd-check4" class="acd-check" type="checkbox">
    <label class="acd-label acd04" for="acd-check4">こだわり条件から探す</label>
    <div class="acd-content">
      <a href="<?php bloginfo('url'); ?>/condition/woman/"><h4 class="sub06">女性・ママ多い</h4></a>
      <a href="<?php bloginfo('url'); ?>/condition/short_time/"><h4 class="sub06">時短勤務OK</h4></a>
	  <a href="<?php bloginfo('url'); ?>/condition/less_days/"><h4 class="sub06">週&#x25CB;日OK</h4></a>
	  <a href="<?php bloginfo('url'); ?>/condition/overseas/"><h4 class="sub06">海外勤務あり</h4></a>
      <a href="<?php bloginfo('url'); ?>/condition/accsess/"><h4 class="sub06">駅近・アクセス&#x25CE;</h4></a>
	  <a href="<?php bloginfo('url'); ?>/condition/commission/"><h4 class="sub06">賞与有・歩合制</h4></a>
	  <a href="<?php bloginfo('url'); ?>/condition/welfare/"><h4 class="sub06">福利厚生&#x25CE;</h4></a>
      <a href="<?php bloginfo('url'); ?>/condition/long_holiday/"><h4 class="sub06">長期休暇あり</h4></a>
	  <a href="<?php bloginfo('url'); ?>/condition/foreigner/"><h4 class="sub06">外国人歓迎</h4></a>
	  <a href="<?php bloginfo('url'); ?>/condition/qualification/"><h4 class="sub06">資格サポート</h4></a>
	</div>
  </div><!-- /acdBlock -->
  
  