<?php
/**
* The template for displaying all single posts
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*
* @package travel_job
*/

get_header();
?>



<div class="main-wid">

  <div class="detail-fixed-btns sp-view">
    <div class="requireList">
      <div class="detail-lineBtn">
        <a href="https://lin.ee/1qtaiQH"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/line-icon.png" width="20px">LINE応募</a>
      </div>
      <form action="/apply-form" method="post">
        <input type="hidden" name="company-tit" value="<?php the_field('company-tit',$post->ID); ?>">
        <input type="hidden" name="url" value="<?php the_permalink(); ?>">
        <input type="submit" value="WEB応募">
      </form>
      <div class="keepBtn">
        <a class="keeplist keeping" href="javascript:void(0);" data-jobid="<?php echo get_the_ID(); ?>">キープ</a>
      </div>
    </div>
  </div>

  <div class="pan">
    <a href="<?php bloginfo('url'); ?>">TOP</a> > <a href="<?php bloginfo('url'); ?>/detail">求人一覧</a> > <?php the_title(); ?>
  </div>
  <div class="side-section">
    <div class="side-col-recDetail"><!-- side-col-recDetail -->
      <?php get_sidebar(2); ?>
    </div>
    <div class="sideSearchBox">
      <form method="get" id="searchform" action="<?php bloginfo('url'); ?>">
        <div>
          <input type="text" name="s" id="s" value="<?php the_search_query(); ?>"  placeholder="キーワードで検索する" />
        </div>
        <div class="text-searchbtn">
          <input type="hidden" name="post_type" value="detail">
          <input type="submit" class="text-searchbtn" value="検索" />
        </div>
      </form>
    </div>
  </div>

  <!--------------------- 修正ここから --------------------->

  <div class="main-col-recDetail sp-pad marb4"><!-- main-col-recDetail -->
    <?php if ( have_posts() ): ?>
      <?php while ( have_posts() ) : the_post(); ?>

        <div class="detail-tit">
          <h1 class="main-col-Ttl"><?php the_title(); ?></h1>
          <div class="heasdAddress"><?php echo post_custom('address1'); ?><?php echo post_custom('address2'); ?></div>
        </div>
        <div class="headingWrap">
          <div class="detailHeading"><!-- detailHeading -->
            <div class="main-slide">
              <?php if( get_field('main-img') ): ?>
                <div><img src="<?php the_field('main-img'); ?>" width="100%" alt=""/></div>
              <?php endif; ?>
              <?php if( get_field('main-img2') ): ?>
                <div><img src="<?php the_field('main-img2'); ?>" width="100%"></div>
              <?php endif; ?>
              <?php if( get_field('main-img3') ): ?>
                <div><img src="<?php the_field('main-img3'); ?>" width="100%"></div>
              <?php endif; ?>
              <?php if( get_field('main-img4') ): ?>
                <div><img src="<?php the_field('main-img4'); ?>" width="100%"></div>
              <?php endif; ?>
            </div>
            <script>
            $('.main-slide').slick({
              arrows:false,
              autoplay:true,
              autoplaySpeed:5000,
              dots:true,
            });
            </script>

            <div class="txt">
              <dl>
                <dt class="type"><span>雇用形態</span></dt>
                <dd><?php echo post_custom('employee'); ?></dd>
              </dl>
              <dl>
                <dt class="salary"><span>給与</span></dt>
                <dd><?php echo post_custom('salarytype'); ?><?php echo post_custom('salary01'); ?>円～<?php if( get_field('salary02') ): ?><?php echo post_custom('salary02'); ?>円<?php endif; ?></dd>
              </dl>
              <dl>
                <dt class="role"><span>募集職種</span></dt>
                <dd><?php echo post_custom('jobtype'); ?></dd>
              </dl>
              <dl>
                <dt class="particular"><span>こだわり</span></dt>
                <dd>
                  <ul>
                    <?php $terms = get_the_terms( get_the_ID(), 'condition' ); ?>
                    <?php if(!empty($terms)): ?>
                      <?php for($i=0; $i<8; $i++): ?>
                        <?php if(isset($terms[$i])): ?>
                          <li><?php echo $terms[$i]->name; ?></li>
                        <?php else: break;?>
                        <?php endif; ?>
                      <?php endfor; ?>
                    <?php endif; ?>
                    <ul>
                    </dd>
                  </dl>

                  <div class="btns">
                    <div class="detail-form-btn">
                      <form action="/apply-form" method="post">
                        <input type="hidden" name="company-tit" value="<?php the_field('company-tit',$post->ID); ?>">
                        <input type="hidden" name="url" value="<?php the_permalink(); ?>">
                        <input type="hidden" name="jobtype" value="<?php the_field('jobtype'); ?>">
                        <input type="hidden" name="employee" value="<?php the_field('employee'); ?>">
                        <input type="submit" value="      WEB応募 | 簡単60秒♪">
                      </form>
                    </div>
                    <div class="keepBtn">
                      <a class="keeplist keeping" href="javascript:void(0);" data-jobid="<?php echo get_the_ID(); ?>">キープ</a>
                    </div>
                    <div class="detail-lineBtn">
                      <a href="https://lin.ee/1qtaiQH"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/line-icon.png" width="20px"> LINE応募</a>
                    </div>
                  </div>
                </div>
              </div><!-- /detailHeading -->

              <div class="detailPoint">
                <dl>
                  <dt><span>この求人の特徴！！</span></dt>
                  <dd>
                    <?php echo post_custom('text'); ?>
                  </dd>
                </dl>
              </div>

              <!-- 会社の特徴・職場の雰囲気 -->
              <div class="detailAtmosGraph">
                <h2 class="comAtmosTitle">会社の特徴・職場の雰囲気</h2>
                <?php if( get_field('img01') ): ?>
                  <div class="detail-3box">
                    <img src="<?php the_field('img01'); ?>" width="100%" class="mart1 marb1">
                    <?php echo post_custom('text02'); ?>
                  </div>
                <?php endif; ?>
                <?php if( get_field('img02') ): ?>
                  <div class="detail-3box">
                    <img src="<?php the_field('img02'); ?>" width="100%" class="mart1 marb1">
                    <?php echo post_custom('text03'); ?>
                  </div>
                <?php endif; ?>
                <?php if( get_field('img03') ): ?>
                  <div class="detail-3box">
                    <img src="<?php the_field('img03'); ?>" width="100%" class="mart1 marb1">
                    <?php echo post_custom('text04'); ?>
                  </div>
                <?php endif; ?>

                <!--
                <table class="graph">
                <tr>
                <th>デスクワーク</th>
                <td>
                <?php if(get_post_meta($post->ID,'number',true) == '1'): ?>
                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar01.png" width="100%">
              <?php elseif (get_post_meta($post->ID,'number',true) == '2'): ?>
              <img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar02.png" width="100%">
            <?php elseif (get_post_meta($post->ID,'number',true) == '3'): ?>
            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar03.png" width="100%">
          <?php elseif (get_post_meta($post->ID,'number',true) == '4'): ?>
          <img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar04.png" width="100%">
        <?php elseif (get_post_meta($post->ID,'number',true) == '5'): ?>
        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar05.png" width="100%">
      <?php endif; ?>
    </td>
    <th>外回り・立ち</th>
  </tr>
</table>
<table class="graph">
<tr>
<th>部署・チーム</th>
<td>
<?php if(get_post_meta($post->ID,'number2',true) == '1'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar01.png" width="100%">
<?php elseif (get_post_meta($post->ID,'number2',true) == '2'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar02.png" width="100%">
<?php elseif (get_post_meta($post->ID,'number2',true) == '3'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar03.png" width="100%">
<?php elseif (get_post_meta($post->ID,'number2',true) == '4'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar04.png" width="100%">
<?php elseif (get_post_meta($post->ID,'number2',true) == '5'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar05.png" width="100%">
<?php endif; ?>
</td>
<th>個人中心</th>
</tr>
</table>
<table class="graph">
<tr>
<th>接客多い</th>
<td>
<?php if(get_post_meta($post->ID,'number3',true) == '1'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar01.png" width="100%">
<?php elseif (get_post_meta($post->ID,'number3',true) == '2'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar02.png" width="100%">
<?php elseif (get_post_meta($post->ID,'number3',true) == '3'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar03.png" width="100%">
<?php elseif (get_post_meta($post->ID,'number3',true) == '4'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar04.png" width="100%">
<?php elseif (get_post_meta($post->ID,'number3',true) == '5'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar05.png" width="100%">
<?php endif; ?>
</td>
<th>接客少ない</th>
</tr>
</table>
<table class="graph">
<tr>
<th>知識経験必要</th>
<td>
<?php if(get_post_meta($post->ID,'number4',true) == '1'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar01.png" width="100%">
<?php elseif (get_post_meta($post->ID,'number4',true) == '2'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar02.png" width="100%">
<?php elseif (get_post_meta($post->ID,'number4',true) == '3'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar03.png" width="100%">
<?php elseif (get_post_meta($post->ID,'number4',true) == '4'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar04.png" width="100%">
<?php elseif (get_post_meta($post->ID,'number4',true) == '5'): ?>
<img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/rankbar05.png" width="100%">
<?php endif; ?>
</td>
<th>知識経験不要</th>
</tr>
</table>
-->
</div>
</div>

<!-- 募集要項 -->
<div class="requireList">
  <h2 class="requireListTitle">募集要項</h2>
  <table>
    <tr>
      <th>会社名</th>
      <td><?php echo post_custom('company-tit'); ?></td>
    </tr>
    <tr>
      <th>勤務地</th>
      <td><?php echo post_custom('address1'); ?><?php echo post_custom('address2'); ?><?php echo post_custom('address3'); ?></td>
    </tr>
    <tr>
      <th>雇用形態</th>
      <td>
        <?php
        $terms = get_the_terms($post->ID,'employee');
        foreach( $terms as $term ) {
          echo $term->name;
        }
        ?>
      </td>
    </tr>
    <tr>
      <th>募集職種</th>
      <td><?php echo post_custom('jobtype'); ?></td>
    </tr>
    <?php if( get_field('work_content') ): ?>
      <tr>
        <th>給与</th>
        <td>
          <p>
            <strong>
              <?php echo post_custom('salarytype'); ?><?php echo post_custom('salary01'); ?>円～<?php if( get_field('salary02') ): ?><?php echo post_custom('salary02'); ?>円<?php endif; ?>
            </strong>
          </p>
          <br/>
          <?php echo post_custom('salary'); ?>
        </tr>
        <tr>
          <th>仕事内容</th>
          <td><?php echo nl2br(get_post_meta($post->ID, 'work_content', true)); ?></td>
        </tr>
      <?php endif; ?>
      <?php if( get_field('model') ): ?>
        <tr>
          <th>求める<br/>人材像</th>
          <td><?php echo nl2br(get_post_meta($post->ID, 'model', true)); ?></td>
        </tr>
      <?php endif; ?>
      <?php if( get_field('item') ): ?>
        <tr>
          <th>主な商品</th>
          <td><?php echo nl2br(get_post_meta($post->ID, 'item', true)); ?></td>
        </tr>
      <?php endif; ?>
      <?php if( get_field('time') ): ?>
        <tr>
          <th>勤務時間</th>
          <td><?php echo nl2br(get_post_meta($post->ID, 'time', true)); ?></td>
        </tr>
      <?php endif; ?>
      <?php if( get_field('holiday') ): ?>
        <tr>
          <th>休日休暇</th>
          <td><?php echo nl2br(get_post_meta($post->ID, 'holiday', true)); ?></td>
        </tr>
      <?php endif; ?>
      <?php if( get_field('access') ): ?>
        <tr>
          <th>アクセス<br/>最寄の駅</th>
          <td><?php echo nl2br(get_post_meta($post->ID, 'access', true)); ?></td>
        </tr>
      <?php endif; ?>
      <?php if( get_field('fukuri') ): ?>
        <tr>
          <th>福利厚生<br/>待遇</th>
          <td><?php echo nl2br(get_post_meta($post->ID, 'fukuri', true)); ?></td>
        </tr>
      <?php endif; ?>
      <?php if( get_field('qualification') ): ?>
        <tr>
          <th>応募資格</th>
          <td><?php echo nl2br(get_post_meta($post->ID, 'qualification', true)); ?></td>
        </tr>
      <?php endif; ?>
      <?php if( get_field('mensetsu') ): ?>
        <tr>
          <th>面接面談<br/>予定地</th>
          <td><?php echo post_custom('mensetsu'); ?></td>
        </tr>
      <?php endif; ?>
      <?php if( get_field('flow') ): ?>
        <tr>
          <th>選考の<br/>フロー</th>
          <td><?php echo nl2br(get_post_meta($post->ID, 'flow', true)); ?></td>
        </tr>
      <?php endif; ?>
    </table>
  </div>

  <div class="btns detail-middle-btns pc-view">
    <div class="detail-lineBtn">
      <a href="https://lin.ee/1qtaiQH"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/line-icon.png" width="20px"> LINE応募</a>
    </div>
    <div class="detail-form-btn">
      <form action="/apply-form" method="post">
        <input type="hidden" name="company-tit" value="<?php the_field('company-tit',$post->ID); ?>">
        <input type="hidden" name="url" value="<?php the_permalink(); ?>">
        <input type="hidden" name="jobtype" value="<?php the_field('jobtype'); ?>">
        <input type="hidden" name="employee" value="<?php the_field('employee'); ?>">
        <input type="submit" value="      WEB応募 | 簡単60秒♪">
      </form>
    </div>
    <div class="keepBtn">
      <a class="keeplist keeping" href="javascript:void(0);" data-jobid="<?php echo get_the_ID(); ?>">キープ</a>
    </div>
  </div>

  <div class="btns detail-middle-btns sp-view">
    <div class="detail-form-btn">
      <form action="/apply-form" method="post">
        <input type="hidden" name="company-tit" value="<?php the_field('company-tit',$post->ID); ?>">
        <input type="hidden" name="url" value="<?php the_permalink(); ?>">
        <input type="hidden" name="jobtype" value="<?php the_field('jobtype'); ?>">
        <input type="hidden" name="employee" value="<?php the_field('employee'); ?>">
        <input type="submit" value="      WEB応募 | 簡単60秒♪">
      </form>
    </div>
    <div class="keepBtn">
      <a class="keeplist keeping" href="javascript:void(0);" data-jobid="<?php echo get_the_ID(); ?>">キープ</a>
    </div>
    <div class="detail-lineBtn">
      <a href="https://lin.ee/1qtaiQH"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/detail/line-icon.png" width="20px"> LINE応募</a>
    </div>
  </div>

  <!-- Google Map -->
  <?php if(empty($value)):?>
  <?php else:?>
    <div class="gmapResponsive">
      <?php echo post_custom('map'); ?>
    </div>
  <?php endif;?>

  <!-- 会社情報 -->
  <?php $relaterms = get_the_terms( get_the_ID(), 'companyname' ); ?>
  <?php if(!empty($relaterms)): ?>
    <div class="detailComInfo">
      <h2 class="detailComInfoTitle">会社情報</h2>
      <table>
        <?php
        $term = array_shift(get_the_terms($post->ID, 'companyname'));
        $args = array(
          'post_type'=>'company',
          'posts_per_page'=>1,
          'orderby'=>'rand',
          'tax_query'=>array(
            array(
              'taxonomy'=>'companyname',
              'field'=>'slug',
              'terms' => $term->slug,
            ),
          ),
        );
        $posts = get_posts( $args );
        if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
        <tr>
          <th>法人名</th>
          <td><a href="<?php the_permalink(); ?>"><?php echo post_custom('companyname'); ?></a></td>
        </tr>
        <tr>
          <th>URL</th>
          <td><a href="<?php echo post_custom('url'); ?>" target="_blank"><?php echo post_custom('url'); ?></a></td>
        </tr>
      <?php endforeach; ?>
    <?php endif; wp_reset_postdata(); ?>
    <tr>
      <th>その他募集中の求人・店舗</th>
      <td>
        <?php
        $term = array_shift(get_the_terms($post->ID, 'companyname'));
        $args = array(
          'post_type'=>'detail',
          'posts_per_page'=>8,
          'orderby'=>'rand',
          'tax_query'=>array(
            array(
              'taxonomy'=>'companyname',
              'field'=>'slug',
              'terms' => $term->slug,
            ),
          ),
        );
        $posts = get_posts( $args );
        if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post ); ?>
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> /
      <?php endforeach; ?>
    <?php endif; wp_reset_postdata(); ?>
  </td>
</tr>
</table>

<?php endif; ?>
<?php endwhile; ?>
<?php endif; ?>

</div>

<div class="recommendRecList"><!-- recommendRecList -->
  <div class="sectionTitle balloon2">
    <h2>この求人と一緒に応募される<br/>おすすめの求人</h2>
  </div>

  <div class="recList"><!-- box -->
    <ul>
      <?php $args = array(
        'post_type'=>'detail',
        'posts_per_page'=>3,
        'orderby'=>'rand',
      );
      $posts = get_posts( $args );
      if( $posts ) : foreach( $posts as $post ) : setup_postdata( $post );
      ?>
      <li><!-- item -->
        <h3><?php the_title(); ?><?php echo post_custom('company'); ?></h3>
        <div class="recDetail">
          <div class="">
            <a href="<?php the_permalink(); ?>">
              <?php if( get_field('main-img') ): ?>
                <figure><img src="<?php the_field('main-img'); ?>" width="282" height="212" alt=""/></figure>
              <?php endif; ?>
            </a>
          </div>
          <div class="txt">
            <dl>
              <dt class="place"><span>勤務地</span></dt>
              <dd><?php echo post_custom('address1'); ?><?php echo post_custom('address2'); ?><?php echo post_custom('address3'); ?></dd>
            </dl>
            <dl>
              <dt class="access"><span>アクセス</span></dt>
              <dd><?php echo nl2br(get_post_meta($post->ID, 'access', true)); ?></dd>
            </dl>
            <dl>
              <dt class="type"><span>雇用形態</span></dt>
              <dd><?php echo post_custom('employee'); ?></dd>
            </dl>
            <dl>
              <dt class="salary"><span>給与</span></dt>
              <dd><?php echo post_custom('salarytype'); ?><?php echo post_custom('salary01'); ?>円～<?php if( get_field('salary02') ): ?><?php echo post_custom('salary02'); ?>円<?php endif; ?></dd>
            </dl>
            <dl>
              <dt class="role"><span>募集職種</span></dt>
              <dd><?php echo post_custom('jobtype'); ?></dd>
            </dl>
            <dl>
              <dt class="particular"><span>こだわり</span></dt>
              <dd>
                <?php $terms = get_the_terms( get_the_ID(), 'condition' ); ?>
                <?php if(!empty($terms)): ?>
                  <?php for($i=0; $i<8; $i++): ?>
                    <?php if(isset($terms[$i])): ?>
                      <?php echo $terms[$i]->name; ?>｜
                    <?php else: break;?>
                    <?php endif; ?>
                  <?php endfor; ?>
                <?php endif; ?>
              </dd>
            </dl>
          </div>
        </div>
        <div class="comment">
          <div class="tx">
            <div class="appealTxt">
              <?php echo post_custom('maincatch'); ?>
            </div>
          </div>
          <div class="btns">
            <div class="keepBtn">
              <a class="keeplist keeping" href="javascript:void(0);" data-jobid="<?php echo get_the_ID(); ?>">キープする</a>
            </div>
            <a href="<?php the_permalink(); ?>">
              <div class="detailBtn">詳しく見る</div>
            </a>
          </div>
        </div>
      </li><!-- /item -->
    <?php endforeach; ?>
  <?php endif; wp_reset_postdata(); ?>
</ul>
</div><!-- /recList -->
</div><!-- /recommendRecList -->
</div><!-- main-col-recDetail -->

</div>

</div>

<!--
<script>
$(function(){
$('.keepBtn').on('click', function(event){
event.preventDefault();
$(this).toggleClass('keeping-img');
});
});
</script>


<!--------------------- 修正ここまで --------------------->

<?php
get_footer();
?>
