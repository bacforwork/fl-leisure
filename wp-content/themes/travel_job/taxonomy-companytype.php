<?php
/**
 * The template for displaying archive of work pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package moumou
 */

get_header();
?>


<div class="main-wid">
	<div class="pan">
		<a href="<?php bloginfo('url'); ?>">TOP</a> > <?php single_term_title(); ?>
	</div>
	<div class="main-col sp-pad">
	<div class="top-tit marb1">
            <h1 class="tit02 tit-def"><?php single_term_title(); ?>の求人一覧</h1>
		</div>
		<?php if (is_mobile()) : ?>
		<?php else: ?>
			<?php get_search_form(); ?>
		<?php endif; ?>

		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="ar-workbox marb4 mart2">
					<a href="<?php the_permalink(); ?>">
					<div class="ar-work-name">
						<p><?php echo post_custom('company'); ?></p>
						<?php the_title(); ?>
					</div>
					</a>
					<div class="ar-workbox-pad">
						<div class="ar-work-catch">
							<?php echo post_custom('maincatch'); ?>
						</div>
						<div class="ar-work-flo">
							<?php if( get_field('main-img') ): ?>
								<a href="<?php the_permalink(); ?>"><img src="<?php the_field('main-img'); ?>" width="100%" class="marb2"></a>
							<?php endif; ?>
							<?php $terms = get_the_terms( get_the_ID(), 'condition' ); ?>
							<?php if(!empty($terms)): ?>
								<?php for($i=0; $i<8; $i++): ?>
									<?php if(isset($terms[$i])): ?>
										<div class="ar-work-term">
											<?php echo $terms[$i]->name; ?>
										</div>
									<?php else: break;?>
									<?php endif; ?>
								<?php endfor; ?>
							<?php endif; ?>
							<div class="clear"></div>
						</div>
						<div class="ar-work-flo2">
							<div class="ar-work-detail">
								<?php
									$text = mb_substr(get_field('artext'),0,80,'utf-8');
									echo $text.'...';
								?>
							</div>
							<div class="ar-work-table marb1">
								<table>
									<tr>
										<th>勤務地</th>
										<td><?php echo post_custom('address1'); ?><?php echo post_custom('address2'); ?></td>
									</tr>
									<tr>
										<th>アクセス</th>
										<td>
											<?php echo nl2br(get_post_meta($post->ID, 'access', true)); ?>
										</td>
									</tr>
									<tr>
										<th>雇用形態</th>
										<td>
											<?php echo post_custom('employee'); ?>
										</td>
									</tr>
									<tr>
										<th>給与</th>
										<td>
											<?php echo post_custom('salarytype'); ?><?php echo post_custom('salary01'); ?>円～<?php if( get_field('salary02') ): ?><?php echo post_custom('salary02'); ?>円<?php endif; ?>
										</td>
									</tr>
									<tr>
										<th>募集職種</th>
										<td>
											<?php echo post_custom('jobtype'); ?>
										</td>
									</tr>
								</table>
							</div>
							<a href="<?php the_permalink(); ?>">
							<div class="ar-work-btn">
								<p>詳しく見る</p>
							</div>
							</a>
                            <p class="btn ar-btn-keep"><a class="keeplist" href="javascript:void(0);" data-jobid="<?php echo get_the_ID(); ?>">キープする</a></p>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			<?php endwhile; ?>
			<div class="pager">
				<?php global $wp_rewrite; $paginate_base = get_pagenum_link(1); if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
					$paginate_format = '';
					$paginate_base = add_query_arg('paged','%#%');
				}
				else{
					$paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
					user_trailingslashit('page/%#%/','paged');;
					$paginate_base .= '%_%';
				}
				echo paginate_links(array(
					'base' => $paginate_base,
					'format' => $paginate_format,
					'total' => $wp_query->max_num_pages,
					'mid_size' => 5,
					'current' => ($paged ? $paged : 1),
					'prev_text' => '«',
					'next_text' => '»',
				)); ?>
			</div>
		<?php else: ?>
			現在<?php single_term_title(); ?>エリアでの求人情報はありません。
		<?php endif; ?>


	</div>

	<div class="side-col">
		<?php get_sidebar(); ?>
	</div>
	<div class="clear"></div>
</div>
<?php
get_footer();

